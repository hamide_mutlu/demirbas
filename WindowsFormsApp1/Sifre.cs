﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Net;
using System.Net.Mail;

namespace WindowsFormsApp1
{
    public partial class Sifre : Form
    {
        public string sifre, Ad, Eposta, kullanıcı;
        public Sifre()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection(@"Data source=.\SQLEXPRESS;Database=demirbas;Integrated Security=true;");


        int randomgenerate;
        string[] letters = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
        Random rand = new Random();
        private void button1_Click(object sender, EventArgs e)
        {
            conn.Open();
            SqlCommand command = new SqlCommand("Select * from Kullanici  where Eposta = '" + textBox1.Text + "'", conn);
            SqlDataReader commandr = command.ExecuteReader();
            string kod = "";
            if (commandr.Read())
            {
                //SqlCommand cmd = new SqlCommand("select count (ID) from Kullanici where KullaniciAdi=@1 and Eposta=@2", conn);
                //cmd.Parameters.AddWithValue("@1", textBox1.Text);
                //cmd.Parameters.AddWithValue("@2", textBox2.Text);
                //conn.Open();
                //int sonuc = (int)cmd.ExecuteScalar();
                //conn.Close();
                //if (sonuc > 0)
                //{

                try
                {
                    DialogResult result = MessageBox.Show("E-posta adresinize onaylama kodu gönderilmesini kabul ediyor musunuz ?", "Doğrulama Penceresi", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (result == DialogResult.Yes)
                    {

                        /// doğrulama için kod oluşturma kısmı

                        do
                        {
                            randomgenerate = rand.Next(0, 10);
                            if (randomgenerate % 2 == 0)
                            {
                                int randomletter = rand.Next(0, 26);
                                kod += letters[randomletter];
                            }
                            else
                            {
                                int random = rand.Next(0, 10);
                                kod += random;
                            }
                        }
                        while (kod.Length < 6);
                        MessageBox.Show(kod); //Oluşturulan Kodu Ekranda Gösterir.
                                              //

                        SmtpClient sc = new SmtpClient();
                        sc.Port = 587;
                        sc.Host = "smtp.gmail.com";
                        sc.EnableSsl = true;



                        sc.Credentials = new NetworkCredential("ffixture05@gmail.com", "fixture123");
                        MailMessage mail = new MailMessage();
                        mail.From = new MailAddress("ffixture05@gmail.com", "Fixture Ekibi");
                        mail.To.Add(textBox1.Text);
                        mail.Subject = "Fixture Hesabınız Hakkında"; // MAİLİN KONUSU
                        mail.IsBodyHtml = true;
                        mail.Body = "Onaylama için doğrulama kodunuz:" + kod;// MAİLİN İÇERİĞİ
                        sc.Send(mail);


                        MessageBox.Show("E-posta mail adresinize gönderildi.\r\rÖnemli bilgi: Eğer doğru e-posta adresinizle bir Fixture hesabı oluşturmadıysanız gönderdiğimiz e-posta başkasının belki de olmayan bir e-posta adresine gidecek ve Fixture hesabınıza birdaha giriş yapamayacaksınız. Böyle bir durumdaysanız sistem yöneticisiyle veya öğretmenseniz alan şefinizle görüşmelisiniz.", "Bildirim Penceresi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        groupBox2.Visible = true;

                    }
                }
                catch
                {
                    MessageBox.Show("Göndermek için girdiğiniz e-posta adresi bulunmamaktadır.\rLütfen girdiğiniz e-posta'yı kontrol edin.", " Hata", MessageBoxButtons.RetryCancel, MessageBoxIcon.Error);
                }
            }
            else
            {
                textBox1.Clear();
                label3.Visible = true;

            }
            conn.Close();
            onaykodu = kod;

        }
        string onaykodu = "";
        private void Button2_Click(object sender, EventArgs e)
        {
            if (onaykodu == textBox2.Text)
            {
                MessageBox.Show("Doğrulama kodunuz doğru.", "Bildirim Penceresi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                groupBox3.Visible = true;
                groupBox2.Visible = false;
            }
            else
            {
                MessageBox.Show("Girdiğiniz doğrulama kodu yanlış.", "Bildirim Penceresi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void PictureBox2_Click(object sender, EventArgs e)
        {
            Giris b = new Giris();
            this.Hide();
            b.Show();
        }
        private void pictureBox3_Click(object sender, EventArgs e)
        {
            DialogResult sondurum = MessageBox.Show("Uygulamayı Kapatmak İstiyormusunuz ?", "Uyarı", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (sondurum == DialogResult.Yes)
            {
                Application.Exit();
            }
        }
        private void pictureBox7_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        bool durum = false;
        private void Button3_Click(object sender, EventArgs e)
        {
            durum = false;
            foreach (var item in şifreler)
            {
                if (textBox3.Text == item)
                {
                    durum = true;
                    break;
                }
            }
            if (durum == false)
            {
                SqlCommand guncelle = new SqlCommand("update Kullanici set Sifre=@sifre where Eposta=@eposta", conn);
                guncelle.Parameters.AddWithValue("@eposta", textBox1.Text);
                guncelle.Parameters.AddWithValue("@sifre", textBox3.Text);
                try
                {
                    conn.Open();
                    guncelle.ExecuteNonQuery();
                    MessageBox.Show("Şifreniz başarıyla güncellendi.\rGiriş ekranına aktarılıyorsunuz.", "Bildirim Penceresi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    conn.Close();
                    Giris open = new Giris();
                    this.Hide();
                    open.Show();
                }
                catch
                {
                    MessageBox.Show("Şifreniz güncellenemedi.\rTekrar deneyin.", "Bildirim Penceresi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else if (durum == true)
            {
                MessageBox.Show("Girdiğiniz şifre kullanılmaktadır. Lütfen başka bir şifre deneyin.", "Bildirim Penceresi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void TextBox3_TextChanged(object sender, EventArgs e)
        {
            if (textBox3.Text == textBox4.Text && textBox3.Text != "" && textBox4.Text != "")
            {
                button3.Enabled = true;
            }
            else
            {
                button3.Enabled = false;
            }
        }


        private void TextBox4_TextChanged_1(object sender, EventArgs e)
        {
            if (textBox3.Text == textBox4.Text)
            {
                button3.Enabled = true;
            }
            else
            {
                button3.Enabled = false;
            }
        }
        List<string> şifreler = new List<string>();
        private void Sifre_Load(object sender, EventArgs e)
        {

            SqlCommand komut = new SqlCommand("select Sifre from Kullanici ", conn);
            conn.Open();
            SqlDataReader dr = komut.ExecuteReader();
            while (dr.Read())
            {
                şifreler.Add(dr["Sifre"].ToString());
            }
            conn.Close();
        }

        private void panel1_MouseDown_1(object sender, MouseEventArgs e)
        {
            mouse_offset = new Point(-e.X, -e.Y);
        }
        private Point mouse_offset;
        private void panel1_MouseMove_1(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                Point mousePos = Control.MousePosition;
                mousePos.Offset(mouse_offset.X, mouse_offset.Y);
                this.Location = mousePos;
            }
        }

    }
}
