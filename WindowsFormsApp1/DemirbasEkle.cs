﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Configuration;
using WindowsFormsApp1.Properties;

namespace WindowsFormsApp1
{
    public partial class DemirbasEkle : Form
    {
        SqlConnection baglanti = new SqlConnection(ConfigurationManager.ConnectionStrings["myConn"].ConnectionString);

        List<string> liste = new List<string>();
        public int rolid { get; set; }
        public int bolumid { get; set; }

        private bool isCollabsed = true;

        public DemirbasEkle()
        {
            InitializeComponent();
        }
        int timerkontrol = 0;

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (timerkontrol == 0)
            {
                flowLayoutPanel1.Width = flowLayoutPanel1.Width - 10;
                if (flowLayoutPanel1.Width == 110)
                {
                    timer1.Stop();
                    timerkontrol = 1;
                }
            }
            else if (timerkontrol == 1)
            {
                flowLayoutPanel1.Width = flowLayoutPanel1.Width + 10;
                if (flowLayoutPanel1.Width == 200)
                {
                    timer1.Stop();
                    timerkontrol = 0;

                    button1.Visible = true;
                    button16.Visible = true;
                    button20.Visible = true;
                    button5.Visible = true;
                    button4.Visible = true;
                    button3.Visible = true;
                    button2.Visible = true;
                    button9.Visible = true;
                    button8.Visible = true;
                    label6.Visible = true;
                }
            }
        }
        private void button6_Click(object sender, EventArgs e)
        {
            if (timerkontrol == 0)
            {
                timer1.Start();

                button16.Visible = false;
                button1.Visible = false;
                button16.Visible = false;
                button20.Visible = false;
                button5.Visible = false;
                button2.Visible = false;
                button3.Visible = false;
                button4.Visible = false;
                button9.Visible = false;
                button8.Visible = false;
                label6.Visible = false;
            }
            else if (timerkontrol == 1)
            {
                timer1.Start();

            }
        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            DialogResult sondurum = MessageBox.Show("Uygulamayı Kapatmak İstiyor musunuz ?", "Bilgilendirme Penceresi", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (sondurum == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void button7_Click_1(object sender, EventArgs e)
        {
            Anasayfa open = new Anasayfa();
            this.Visible = false;
            open.rolid = rolid;
            open.bolumid = bolumid;
            open.Visible = true;
            open.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            KategoriEkle open = new KategoriEkle();
            this.Visible = false;
            open.rolid = rolid;
            open.bolumid = bolumid;
            open.Visible = true;
            open.Show();
        }

        List<Control> kontroller = new List<Control>();

        private void güncelleToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
        private void button4_Click(object sender, EventArgs e)
        {
            Kullanıcı open = new Kullanıcı();
            this.Visible = false;
            open.rolid = rolid;
            open.bolumid = bolumid;
            open.ShowDialog();
            //
        }
        private Point mouse_offset;
        private void panel2_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                Point mousePos = Control.MousePosition;
                mousePos.Offset(mouse_offset.X, mouse_offset.Y);
                this.Location = mousePos;
            }
        }

        private void panel2_MouseDown(object sender, MouseEventArgs e)
        {

            mouse_offset = new Point(-e.X, -e.Y);
        }
      
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            textBox1.Clear();
            label5.Text = "Kodun Geleceği Yer";
            label1.Text = "Özellik";
            button12.Enabled = false;
            listView1.Items.Clear();

            SqlCommand kontrol3 = new SqlCommand("select t3.ID,t3.ozellikAdı,t1.kisaKod,kategoriID from Kategori t1 inner join KategoriOzellik t2 on t1.ID=t2.kategoriID " +
            "inner join Ozellik t3 on t2.ozellikID = t3.ID where t1.ID = '" + comboBox1.SelectedValue + "'", baglanti);


            baglanti.Open();
            SqlDataReader dr4 = kontrol3.ExecuteReader();
            while (dr4.Read())
            {
                ListViewItem item = new ListViewItem(dr4[1].ToString());
                item.SubItems.Add(dr4[0].ToString());
                item.SubItems.Add("");
                listView1.Items.Add(item);
                label5.Text = dr4[2].ToString();
            }
            baglanti.Close();

            if (comboBox1.SelectedIndex==0 &&comboBox1.Text=="Seçiniz")
            {
                groupBox2.Text = " Özellikleri";
            }

            //if (comboBox1.Text == "Seçiniz")
            //{
            //    groupBox2.Text = " Özellikleri";
            //}
            else
            {
                groupBox2.Text = comboBox1.Text + " Özellikleri";
            }



            //string kod = string.Empty;

            if (comboBox1.SelectedIndex > 0)
            {
                baglanti.Close();
                SqlCommand kontrol = new SqlCommand("select count(*) from Kategori t1 inner join KategoriOzellik t2 on t1.ID = t2.kategoriID inner join Ozellik t3 on t3.ID = t2.ozellikID where t1.ID=@id", baglanti);

                kontrol.Parameters.AddWithValue("@id", comboBox1.SelectedValue);
                baglanti.Open();
                int cevap = 0;
                SqlDataReader dr5 = kontrol.ExecuteReader();
                while (dr5.Read())
                {
                    cevap = Convert.ToInt32(dr5[0]);
                }

                baglanti.Close();
                string[] colisimleri = new string[cevap];

                SqlCommand komut3 = new SqlCommand("select t3.ozellikAdı from Kategori t1 inner join KategoriOzellik t2 on t1.ID = t2.kategoriID inner join Ozellik t3 on t3.ID = t2.ozellikID where t1.ID=@id", baglanti);
                komut3.Parameters.AddWithValue("@id", comboBox1.SelectedValue);
                baglanti.Open();
                SqlDataReader dr2 = komut3.ExecuteReader();
                int sayac = 0;
                while (dr2.Read())
                {
                    colisimleri[sayac] = dr2[0].ToString();
                    sayac++;
                }
                baglanti.Close();
                comboBox2.Enabled = true;
                comboBox2.Visible = true;
                label8.Visible = true;
            }
            else
            {
                comboBox2.Enabled = false;
                comboBox2.Visible = false;
                label8.Visible = false;
            }
            baglanti.Close();
        }



        private void Demirbas_Load(object sender, EventArgs e)
        {
            if (rolid == 1)
            {
                label7.Visible = false;
                comboGetir();
            }
            if (rolid == 2)
            {
                baglanti.Open();
                mekan = new SqlCommand("select * from Mekan where BolumID='" + bolumid + "' order by Yer ASC", baglanti);
                SqlDataReader dr = mekan.ExecuteReader();
                Dictionary<string, string> dic = new Dictionary<string, string>();
                dic.Add("", "Seçiniz");
                while (dr.Read())
                {
                    dic.Add(dr.GetValue(0).ToString(), dr.GetValue(1).ToString());
                }
                comboBox2.DataSource = new BindingSource(dic, null);
                comboBox2.DisplayMember = "Value";
                comboBox2.ValueMember = "Key";
                comboBox2.Text = "";
                dr.Close();
                baglanti.Close();
            }

            
            textBox1.Enabled = false;
            button12.Enabled = false;

            SqlCommand sorgu = new SqlCommand("select Ad  from Kullanici where Rolid=@rolid", baglanti);
            sorgu.Parameters.AddWithValue("@rolid", rolid);

            baglanti.Open();
            string sonuc = (string)sorgu.ExecuteScalar();
            baglanti.Close();

            if (rolid == 2)
            {
                button20.Enabled = true;
                button1.Enabled = true;
                button2.Enabled = true;
                button4.Enabled = true;
                button5.Visible = false;
                button16.Enabled = true;
                comboBox4.Visible = false;
                label4.Text = "Bölümünüz : ";
                baglanti.Open();
                SqlCommand bolumad = new SqlCommand("select BolumAdı from Bolumler as b " +
                    "inner join Kullanici as k on k.Bolumid=b.BolumID where BolumID='" + bolumid + "'", baglanti);

                SqlDataReader dr = bolumad.ExecuteReader();
                if (dr.Read())
                {
                    label7.Text = dr.GetValue(0).ToString();
                }
                dr.Close();
                baglanti.Close();


                baglanti.Open();

                SqlCommand komut3 = new SqlCommand("select ID,kategoriAd from Kategori where BolumID =" + bolumid, baglanti);
                SqlDataReader dr3 = komut3.ExecuteReader();
                Dictionary<string, string> dis2 = new Dictionary<string, string>();

                dis2.Add("", "Seçiniz");
                while (dr3.Read())
                {
                    dis2.Add(dr3.GetValue(0).ToString(), dr3.GetValue(1).ToString());
                }
                comboBox1.SelectedIndexChanged -= comboBox1_SelectedIndexChanged;
                comboBox1.DataSource = new BindingSource(dis2, null);
                comboBox1.DisplayMember = "Value";
                comboBox1.ValueMember = "Key";
                comboBox1.Text = "";
                comboBox1.SelectedIndexChanged += comboBox1_SelectedIndexChanged;
                dr3.Close();

                baglanti.Close();
            }
            else if (rolid == 3)
            {
                button16.Enabled = false;
                button20.Enabled = false;
                button1.Enabled = false;
                button2.Enabled = false;
                button4.Enabled = false;
                button5.Enabled = false;
                button3.Enabled = false;
                label7.Visible = false;
                comboBox1.Enabled = false;
            }

        }

        public void comboGetir()
        {
            baglanti.Open();
            SqlCommand komut2 = new SqlCommand("select*from Bolumler", baglanti);
            SqlDataAdapter da = new SqlDataAdapter(komut2);
            SqlDataReader dr2 = komut2.ExecuteReader();
            Dictionary<string, string> dis = new Dictionary<string, string>();

            dis.Add("", "Seçiniz");
            while (dr2.Read())
            {
                dis.Add(dr2.GetValue(0).ToString(), dr2.GetValue(1).ToString());
            }
            comboBox4.DataSource = new BindingSource(dis, null);
            comboBox4.DisplayMember = "Value";
            comboBox4.ValueMember = "Key";
            dr2.Close();
            baglanti.Close();
        }
        private void button11_Click(object sender, EventArgs e)
        {
            DemirbasAyarlar ayar = new DemirbasAyarlar();
            this.Hide();
            ayar.rolid = rolid;
            ayar.Show();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Mekan sa = new Mekan();
            this.Visible = false;
            sa.rolid = rolid;
            sa.bolumid = bolumid;
            sa.ShowDialog();
        }
        int sayac6 = 0;
        private void button10_Click(object sender, EventArgs e)
        {
            durum = true;
            if (sayac6 >= 1)
            {
                if (comboBox2.SelectedIndex > 0)
                {
                    baglanti.Open();

                    string kod2 = Guid.NewGuid().ToString().Substring(0, 8);

                    string kisakod = "";
                    SqlCommand kmt2 = new SqlCommand("select kisaKod from Kategori where ID = '" + comboBox1.SelectedValue + "'", baglanti);
                    SqlDataReader dr = kmt2.ExecuteReader();
                    if (dr.Read())
                    {
                        kisakod = dr["kisaKod"].ToString();
                        //  MessageBox.Show(ürünkodu);
                    }
                    var ürünkodu = kisakod + kod2;


                    baglanti.Close();


                    List<Tuple<string, string>> veri = new List<Tuple<string, string>>();
                    int sayac = 0;
                    baglanti.Open();

                    SqlCommand kmt = new SqlCommand("insert into Urunler (kategoriID,ürünkodu,mekanID) values(@p1,@p2,@p3)", baglanti);
                    kmt.Parameters.AddWithValue("@p1", comboBox1.SelectedValue);
                    kmt.Parameters.AddWithValue("@p2", ürünkodu.ToString());
                    kmt.Parameters.AddWithValue("@p3", comboBox2.SelectedValue);
                    kmt.ExecuteNonQuery();//demirbaş eklendi.
                    SqlCommand komut = new SqlCommand("select distinct UrunID,ozellikID from Urunler t0 " +
                                                   "inner join KategoriOzellik t1 on t0.kategoriID = t1.kategoriID " +
                                                   "where t0.ürünkodu = '" + ürünkodu + "'", baglanti);
                    SqlDataReader dr2 = komut.ExecuteReader();

                    while (dr2.Read())
                    {
                        veri.Add(new Tuple<string, string>(dr2[0].ToString(), dr2[1].ToString()));//özellikler listeye aktarıldı.
                    }
                    dr2.Close();
                    baglanti.Close();
                    foreach (ListViewItem item in listView1.Items)
                    {
                        baglanti.Open();

                        if (item.SubItems[2].Text != "")
                        {
                            SqlCommand komut2 = new SqlCommand("insert into Urün_Ozellik (urunID,ozellikID,ozellikDegeri) values(@urun,@ozellik,@deger)", baglanti);
                            komut2.Parameters.AddWithValue("@urun", veri[sayac].Item1);
                            komut2.Parameters.AddWithValue("@ozellik", veri[sayac].Item2);
                            komut2.Parameters.AddWithValue("@deger", item.SubItems[2].Text);

                            komut2.ExecuteNonQuery();
                        }
                        baglanti.Close();
                        sayac++;
                    }


                    MessageBox.Show("Ürün eklenmiştir", "Bilgilendirme Penceresi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    if (baglanti.State == ConnectionState.Open)
                    {
                        baglanti.Close();
                    }
                    ////////////////////////////////
                    textBox1.Clear();
                    button10.Enabled = false;
                    for (int i = 0; i < listView1.Items.Count; i++)
                    {
                        listView1.Items[i].SubItems[2].Text = "";
                    }

                    groupBox2.Text = comboBox1.Text + " Özellikleri";

                    //DemirbasEkle open = new DemirbasEkle();
                    //this.Visible = false;
                    //open.rolid = rolid;
                    //open.bolumid = bolumid;
                    //open.Visible = true;
                }
                else
                {
                    MessageBox.Show("Mekan seçmeden demirbaş ekletemezsiniz.", "Bilgilendirme Penceresi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            else
            {
                MessageBox.Show("En az bir özelliğe veri eklemeniz gerekiyor.", "Bilgilendirme Penceresi", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }



        }

        private void Button3_Click(object sender, EventArgs e)
        {

        }

        private void Button5_Click(object sender, EventArgs e)
        {
            Bolumler sa = new Bolumler();
            this.Visible = false;
            sa.rolid = rolid;
            sa.bolumid = bolumid;
            sa.ShowDialog();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
        SqlCommand mekan = new SqlCommand();
        private void ComboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {
            label5.Text = "Kodun Geleceği Yer";
            label1.Text = "Özellik";
            textBox1.Clear();
            button12.Enabled = false;
            listView1.Items.Clear();
            if (comboBox4.SelectedIndex == 0)
            {
                groupBox2.Text = "Özellikler";
                comboBox1.Enabled = false;

            }
            else
            {
                groupBox2.Text = "Özellikler";
                comboBox1.Enabled = true;
            }


            if (comboBox4.SelectedIndex > 0)
            {
                baglanti.Open();
                if (comboBox1.SelectedIndex > 0)
                {
                    comboBox2.Enabled = true;
                    comboBox2.Visible = true;
                    label8.Visible = true;
                }
                SqlCommand komut3 = new SqlCommand("select ID,kategoriAd from Kategori where BolumID =" + comboBox4.SelectedValue, baglanti);
                SqlDataReader dr3 = komut3.ExecuteReader();
                Dictionary<string, string> dis2 = new Dictionary<string, string>();

                dis2.Add("", "Seçiniz");
                while (dr3.Read())
                {
                    dis2.Add(dr3.GetValue(0).ToString(), dr3.GetValue(1).ToString());
                }
                comboBox1.SelectedIndexChanged -= comboBox1_SelectedIndexChanged;
                comboBox1.DataSource = new BindingSource(dis2, null);
                comboBox1.DisplayMember = "Value";
                comboBox1.ValueMember = "Key";
                comboBox1.Text = "";
                comboBox1.SelectedIndexChanged += comboBox1_SelectedIndexChanged;
                dr3.Close();

                baglanti.Close();

                baglanti.Open();

                if (rolid == 1)
                {
                    mekan = new SqlCommand("select * from Mekan where BolumID='" + comboBox4.SelectedValue + "' order by Yer ASC", baglanti);


                    SqlDataReader dr = mekan.ExecuteReader();
                    Dictionary<string, string> dic = new Dictionary<string, string>();
                    dic.Add("", "Seçiniz");
                    while (dr.Read())
                    {
                        dic.Add(dr.GetValue(0).ToString(), dr.GetValue(1).ToString());
                    }
                    comboBox2.DataSource = new BindingSource(dic, null);
                    comboBox2.DisplayMember = "Value";
                    comboBox2.ValueMember = "Key";
                    comboBox2.Text = "";
                    dr.Close();
                    baglanti.Close();
                }
            }
            else
            {
                comboBox2.Enabled = false;
                comboBox2.Visible = false;
                label8.Visible = false;
            }

        }

        private void Button8_Click(object sender, EventArgs e)
        {
            DemirbasAyarlar ayar = new DemirbasAyarlar();
            this.Hide();
            ayar.rolid = rolid;
            ayar.bolumid = bolumid;
            ayar.ShowDialog();
        }

        private void Timer2_Tick(object sender, EventArgs e)
        {
            if (isCollabsed)
            {
                button9.Image = Resources.up_arrow;
                panelDropDown.Height += 10;
                if (panelDropDown.Size == panelDropDown.MaximumSize)
                {
                    timer2.Stop();
                    isCollabsed = false;
                }
            }
            else
            {
                button9.Image = Resources.angle_arrow_down;
                panelDropDown.Height -= 10;
                if (panelDropDown.Size == panelDropDown.MinimumSize)
                {
                    timer2.Stop();
                    isCollabsed = true;
                }
            }
        }

        private void Button9_Click(object sender, EventArgs e)
        {
            timer2.Start();
        }

        private void listView1_ColumnWidthChanged(object sender, ColumnWidthChangedEventArgs e)
        {
            if (listView1.Columns[1].Width > 0)
            {
                listView1.Columns[1].Width = 0;
            }

        }
        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (listView1.SelectedItems.Count > 0)
            {
                label1.Text = listView1.SelectedItems[0].SubItems[0].Text + " ";
                textBox1.Enabled = true;
                label1.Visible = true;
                button12.Enabled = true;
                //label1.Text = "Özellikler";
            }
        }
        bool durum = true;
        private void button12_Click(object sender, EventArgs e)
        {

            if (textBox1.Text == "")
            {
                MessageBox.Show("Lütfen alanları boş bırakmayın.", "Bilgilendirme Penceresi",MessageBoxButtons.OK,MessageBoxIcon.Warning);
            }
            else
            {

                if (listView1.SelectedItems.Count > 0)
                {
                    listView1.SelectedItems[0].SubItems[2].Text = textBox1.Text;
                    textBox1.Text = "";
                }
                sayac6++;

                if (durum == true)
                {
                    groupBox2.Text += " *";

                    durum = false;
                    button10.Enabled = true;
                    textBox1.Focus();
                }
            }
        }

        private void button3_Click_1(object sender, EventArgs e)
        {

        }

        private void pictureBox7_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void ComboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {
            DialogResult sondurum = MessageBox.Show("Kullanıcı değiştirmek istiyor musunuz ?", "Bilgilendirme Penceresi", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (sondurum == DialogResult.Yes)
            {
                Giris open = new Giris();
                this.Visible = false;
                open.ShowDialog();
            }
        }

        private void button16_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Daha yapılmadı");
        }

        private void button20_Click(object sender, EventArgs e)
        {
            Anasayfa open = new Anasayfa();
            this.Visible = false;
            open.bolumid = bolumid;
            open.rolid = rolid;
            open.ShowDialog();
        }
    }
}
