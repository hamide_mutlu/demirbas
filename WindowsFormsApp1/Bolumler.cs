﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Configuration;
using WindowsFormsApp1.Properties;

namespace WindowsFormsApp1
{
    public partial class Bolumler : Form
    {
        public Bolumler()
        {
            InitializeComponent();
        }
        SqlConnection baglanti = new SqlConnection(ConfigurationManager.ConnectionStrings["myConn"].ConnectionString);
        public int bolumid { get; set; }
        public int rolid { get; set; }
        private bool isCollabsed = true;


        private void Panel1_Paint(object sender, PaintEventArgs e)
        {
        }
        private void Bolumler_Load(object sender, EventArgs e)
        {
            //listView1.Columns.Add("Bolum Adı", 320);
            //listView1.Columns.Add("ID", 0);
            listView1.CheckBoxes = true;
            listView1.FullRowSelect = true;
            button15.Enabled = false;
            Getir();

            SqlCommand sorgu = new SqlCommand("select Ad  from Kullanici where Rolid=@rolid", baglanti);
            sorgu.Parameters.AddWithValue("@rolid", rolid);

            baglanti.Open();
            string sonuc = (string)sorgu.ExecuteScalar();
            baglanti.Close();

            if (rolid == 2)
            {
                button20.Enabled = true;
                button12.Enabled = true;
                button10.Enabled = true;
                button11.Enabled = true;
                button9.Enabled = true;
                button13.Visible = false;
                button4.Enabled = false;
                button16.Enabled = true;
            }
            else if (rolid == 3)
            {
                button17.Enabled = false;
                button20.Enabled = false;
                button7.Enabled = false;
                button3.Enabled = true;
                button2.Enabled = true;
                button4.Enabled = false;
                button16.Enabled = false;
            }
        }
        private void Getir()
        {
            SqlCommand komut = new SqlCommand("select BolumID,BolumAdı from Bolumler", baglanti);
            baglanti.Open();
            SqlDataReader dr = komut.ExecuteReader();
            while (dr.Read())
            {
                ListViewItem item = new ListViewItem(dr["BolumAdı"].ToString());
                item.SubItems.Add(dr["BolumID"].ToString());
                listView1.Items.Add(item);
            }
            textBox5.Clear();

            textBox5.Enabled = false;
            baglanti.Close();
        }

        private void PictureBox6_Click(object sender, EventArgs e)
        {
            DialogResult sondurum = MessageBox.Show("Uygulamayı Kapatmak İstiyor musunuz ?", "Bilgilendirme Penceresi", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (sondurum == DialogResult.Yes)
            {
                Application.Exit();
            }
        }
        private void Button10_Click(object sender, EventArgs e)
        {

        }
        private void Button9_Click(object sender, EventArgs e)
        {
            Kullanıcı open = new Kullanıcı();
            open.rolid = rolid;
            open.bolumid = bolumid;
            this.Visible = false;
            open.Visible = true;
            open.Show();
        }
        private void Button12_Click(object sender, EventArgs e)
        {
            KategoriEkle open = new KategoriEkle();
            this.Visible = false;
            open.Visible = false;
            open.rolid = rolid;
            open.bolumid = bolumid;
            open.ShowDialog();
        }

        private void Button11_Click(object sender, EventArgs e)
        {
        }
        private void Button13_Click(object sender, EventArgs e)
        {
            Bolumler b = new Bolumler();
            this.Visible = false;
            b.rolid = rolid;
            b.bolumid = bolumid;
            b.ShowDialog();
        }
        private void Button8_Click(object sender, EventArgs e)
        {
            Anasayfa sa = new Anasayfa();
            this.Visible = false;
            sa.rolid = rolid;
            sa.bolumid = bolumid;
            sa.ShowDialog();
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {
        }
        private Point mouse_offset;
        private void panel2_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                Point mousePos = Control.MousePosition;
                mousePos.Offset(mouse_offset.X, mouse_offset.Y);
                this.Location = mousePos;
            }
        }
        private void panel2_MouseDown(object sender, MouseEventArgs e)
        {
            mouse_offset = new Point(-e.X, -e.Y);
        }
        private void button11_Click_1(object sender, EventArgs e)
        {
            Mekan sa = new Mekan();
            this.Visible = false;
            sa.rolid = rolid;
            sa.bolumid = bolumid;
            sa.Visible = false;
            sa.ShowDialog();
        }

        private void button16_Click(object sender, EventArgs e)
        {
           
        }
        private void listView1_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            //    for (int i = 0; i <listView1.SelectedItems.Count; i++)
            //    {

            //        textBox5.Text = listView1.SelectedItems[i].SubItems[0].Text;
            //    }
            //    textBox5.Enabled = true;
            //    button15.Enabled = true;
        }
        private void listView1_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            label3.Text = "Seçilen Bölüm Sayısı:" + listView1.CheckedItems.Count;

            if (listView1.CheckedItems.Count == 0)
            {
                if (textBox5.Text != "")
                {
                    textBox5.Enabled = true;
                    button15.Enabled = true;
                }
                button14.Enabled = false;
            }
            else if (listView1.CheckedItems.Count == 1)
            {
                textBox5.Enabled = false;
                button15.Enabled = false;
                button14.Enabled = true;
            }
            else if (listView1.CheckedItems.Count > 1)
            {
                textBox5.Enabled = false;
                button15.Enabled = false;
                button14.Enabled = true;
            }
        }
        private void button15_Click(object sender, EventArgs e)
        {
            guncelle();
            button15.Enabled = false;
        }
        string kontrol = "";
        private void guncelle()
        {
            string ad = textBox5.Text;

            kontrol = ad;
            if (kontrol != ad)
            {
                for (int i = 0; i < 1; i++)
                {
                    SqlCommand guncelle = new SqlCommand("update Bolumler set BolumAdı=@bolumadı where BolumID=@ıd", baglanti);

                    guncelle.Parameters.AddWithValue("@ıd", listView1.SelectedItems[i].SubItems[1].Text);
                    guncelle.Parameters.AddWithValue("@bolumadı", textBox5.Text);
                    //değişiklik
                    try
                    {
                        baglanti.Open();
                        guncelle.ExecuteNonQuery();
                        baglanti.Close();
                        button5.Enabled = true;

                        listView1.Items.Clear();
                        Getir();
                        listView1.CheckBoxes = true;

                        MessageBox.Show("Başarıyla güncellendi", "Bilgilendirme Penceresi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (Exception ex)
                    {
                        baglanti.Close();
                        MessageBox.Show("Kayıt Güncellenemedi.", "Başarısız!");
                        MessageBox.Show("" + ex);
                    }
                }
            }
            else
            {
                MessageBox.Show("Bölümün adını değiştirmediniz.", "Bilgilendirme", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
         
        }
        List<int> oid = new List<int>();
        List<int> kid = new List<int>();
        private void button14_Click(object sender, EventArgs e)
        {

            DialogResult cikis;
            bool seçmekontrol = false;
            if (listView1.CheckedItems.Count == 1)
            {
                cikis = MessageBox.Show("Seçilen bölümü silmek istiyor musunuz?", "Bilgilendirme Penceresi", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                seçmekontrol = true;
            }
            else
            {
                cikis = MessageBox.Show("Seçilen bölümleri silmek istiyor musunuz?", "Bilgilendirme Penceresi", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            }
            if (cikis == DialogResult.Yes)
            {

                SqlCommand komut = new SqlCommand("select * from Bolumler as b " +
                    "inner join Kategori as k on b.BolumID = k.bolumID " +
                    "inner join KategoriOzellik as ko on ko.kategoriID = k.ID where b.BolumID='" + listView1.CheckedItems[0].SubItems[1].Text + "'", baglanti);
                baglanti.Open();
                SqlDataReader dr = komut.ExecuteReader();


                while (dr.Read())
                {
                    oid.Add((int)dr["ozellikID"]);
                    kid.Add((int)dr["kategoriID"]);
                }
                dr.Close();
                baglanti.Close();
                foreach (var item in oid)
                {
                    ///////// kategori özellik tablsoundan özellikid sildiriyorum
                    SqlCommand kategoriozellik = new SqlCommand("delete from KategoriOzellik where ozellikID= @id", baglanti);
                    kategoriozellik.Parameters.AddWithValue("@id", item);
                    baglanti.Open();
                    kategoriozellik.ExecuteNonQuery();
                    baglanti.Close();
                    ///

                    ///////urun özellik tablosundan özellik id sildiriyorum
                    SqlCommand urunozellik = new SqlCommand("delete from Urün_Ozellik where ozellikID= @id", baglanti);
                    urunozellik.Parameters.AddWithValue("@id", item);
                    baglanti.Open();
                    urunozellik.ExecuteNonQuery();
                    baglanti.Close();
                    ///

                    ///////ozellik tablosundan ozellik sildiriyorum
                    SqlCommand ozellik = new SqlCommand("delete from Ozellik where ID=@id", baglanti);
                    ozellik.Parameters.AddWithValue("@id", item);
                    baglanti.Open();
                    ozellik.ExecuteNonQuery();
                    baglanti.Close();
                    ///
                }

                foreach (var item2 in kid)
                {
                    SqlCommand urun = new SqlCommand("delete from Urunler where kategoriID= @id", baglanti);
                    urun.Parameters.AddWithValue("@id", item2);
                    baglanti.Open();
                    urun.ExecuteNonQuery();
                    baglanti.Close();
                }





                for (int i = 0; i < listView1.CheckedItems.Count; i++)
                {
                    SqlCommand odev3 = new SqlCommand("delete from Kategori where bolumID= @id", baglanti);
                    odev3.Parameters.AddWithValue("@id", listView1.CheckedItems[i].SubItems[1].Text);
                    baglanti.Open();
                    odev3.ExecuteNonQuery();
                    baglanti.Close();

                    SqlCommand odev2 = new SqlCommand("delete from Mekan where BolumID= @id", baglanti);
                    odev2.Parameters.AddWithValue("@id", listView1.CheckedItems[i].SubItems[1].Text);
                    baglanti.Open();
                    odev2.ExecuteNonQuery();
                    baglanti.Close();

                    SqlCommand odev = new SqlCommand("delete from Bolumler where BolumID= @id", baglanti);
                    odev.Parameters.AddWithValue("@id", listView1.CheckedItems[i].SubItems[1].Text);
                    baglanti.Open();
                    odev.ExecuteNonQuery();
                    baglanti.Close();
                }
                listView1.Items.Clear();
                Getir();
                if (seçmekontrol == true)
                {
                    MessageBox.Show("Seçilen bölüm silindi.", "Bilgilendirme Penceresi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Seçilen bölümler silindi.", "Bilgilendirme Penceresi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                textBox5.Clear();
            }
        }
        private void Panel4_Paint(object sender, PaintEventArgs e)
        {
        }
        private void Label2_Click(object sender, EventArgs e)
        {
        }

        int timerkontrol = 0;

        private void Button5_Click(object sender, EventArgs e)
        {
            if (timerkontrol == 0)
            {
                timer1.Start();


                button20.Visible = false;
                button16.Visible = false;
                button12.Visible = false;
                button13.Visible = false;
                button10.Visible = false;
                button11.Visible = false;
                button9.Visible = false;
                button17.Visible = false;
                button18.Visible = false;
                label5.Visible = false;
                button16.Visible = false;
            }
            else if (timerkontrol == 1)
            {
                timer1.Start();

            }
        }

        private void Timer1_Tick(object sender, EventArgs e)
        {

            if (timerkontrol == 0)
            {
                flowLayoutPanel1.Width = flowLayoutPanel1.Width - 10;
                if (flowLayoutPanel1.Width == 110)
                {
                    timer1.Stop();
                    timerkontrol = 1;
                }
            }
            else if (timerkontrol == 1)
            {
                flowLayoutPanel1.Width = flowLayoutPanel1.Width + 10;
                if (flowLayoutPanel1.Width == 200)
                {
                    timer1.Stop();
                    timerkontrol = 0;
                    button20.Visible = true;
                    button16.Visible = true;
                    button12.Visible = true;
                    button10.Visible = true;
                    button11.Visible = true;
                    button9.Visible = true;
                    button13.Visible = true;
                    panel3.Visible = true;
                    button17.Visible = true;
                    button18.Visible = true;
                    label5.Visible = true;

                }
            }
        }

        private void Timer2_Tick(object sender, EventArgs e)
        {
            if (isCollabsed)
            {
                button18.Image = Resources.up_arrow;
                panelDropDown.Height += 10;
                if (panelDropDown.Size == panelDropDown.MaximumSize)
                {
                    timer2.Stop();
                    isCollabsed = false;
                }
            }
            else
            {
                button18.Image = Resources.angle_arrow_down;
                panelDropDown.Height -= 10;
                if (panelDropDown.Size == panelDropDown.MinimumSize)
                {
                    timer2.Stop();
                    isCollabsed = true;
                }
            }
        }

        private void Button18_Click(object sender, EventArgs e)
        {
            timer2.Start();
        }

        private void Button17_Click(object sender, EventArgs e)
        {
            DemirbasEkle open = new DemirbasEkle();
            this.Visible = false;

            open.rolid = rolid;
            open.bolumid = bolumid;
            open.ShowDialog();
        }

        private void Button10_Click_1(object sender, EventArgs e)
        {
            DemirbasAyarlar ayar = new DemirbasAyarlar();
            this.Hide();
            ayar.rolid = rolid;
            ayar.bolumid = bolumid;
            ayar.ShowDialog();
        }

        private void pictureBox10_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count > 0)
            {
                textBox5.Clear();

                textBox5.Enabled = true;
                textBox5.Text = listView1.SelectedItems[0].SubItems[0].Text;
                button15.Enabled = true;
                //textBox5.Focus();
            }
            else
            {
                textBox5.Clear();
                textBox5.Enabled = false;
                button15.Enabled = false;
            }

        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {
            DialogResult sondurum = MessageBox.Show("Kullanıcı değiştirmek istiyor musunuz ?", "Bilgilendirme Penceresi", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (sondurum == DialogResult.Yes)
            {
                Giris open = new Giris();
                this.Visible = false;
                open.ShowDialog();
            }
        }

        private void Button19_Click(object sender, EventArgs e)
        {
            bool durum = false;


            if (textBox2.Text != "")
            {
                if (listView1.Items.Count > 0)
                {
                    foreach (var kontrol in listView1.Items)
                    {
                        if (textBox2.Text == kontrol.ToString())
                        {
                            durum = true;
                        }

                    }
                }
                if (!durum)
                {
                    string sorgu = "insert into Bolumler(BolumAdı)values(@bolum)";
                    SqlCommand komut = new SqlCommand(sorgu, baglanti);
                    komut.Parameters.AddWithValue("@bolum", textBox2.Text);

                    int kodvarmi = 0;
                    SqlCommand kodbak = new SqlCommand("select BolumAdı from Bolumler", baglanti);
                    baglanti.Open();
                    SqlDataReader kobak = kodbak.ExecuteReader();
                    while (kobak.Read())
                    {

                        if (textBox2.Text.ToUpper() == kobak["BolumAdı"].ToString().ToUpper())
                            kodvarmi++;

                    }

                    kobak.Close();
                    if (kodvarmi > 0)
                    {
                        MessageBox.Show("Bu bölüm adı zaten kullanımda.", "Bilgilendirme Penceresi", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        if (baglanti.State == ConnectionState.Open)
                        {
                            baglanti.Close();
                        }
                    }
                    else
                    {
                        try
                        {
                            if (baglanti.State == ConnectionState.Closed)
                            {
                                baglanti.Open();
                            }
                            komut.ExecuteNonQuery();
                            if (baglanti.State == ConnectionState.Open)
                            {
                                baglanti.Close();
                            }
                            listView1.Items.Clear();
                            Getir();
                            MessageBox.Show(textBox2.Text + " bölümü eklenmiştir.", "Bilgilendirme Penceresi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            textBox2.Clear();
                        }
                        catch (Exception hata)
                        {
                            baglanti.Close();
                            MessageBox.Show("Kaydınız Veri Tabanına eklenemememiştir " + hata.Message);
                        }
                    }

                }
                else
                {
                    MessageBox.Show("Boş bölüm ekletilemez.", "Bilgilendirme Penceresi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {

                MessageBox.Show("Lütfen bir bölüm adı giriniz.", "Bilgilendirme Penceresi", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }

        }

        private void button16_Click_1(object sender, EventArgs e)
        {
            MessageBox.Show("Daha yapılmadı");
        }

        private void button20_Click(object sender, EventArgs e)
        {
            Anasayfa open = new Anasayfa();
            this.Visible = false;
            open.bolumid = bolumid;
            open.rolid = rolid;
            open.ShowDialog();
        }
    }
}
