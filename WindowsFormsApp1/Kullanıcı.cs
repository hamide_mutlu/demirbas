﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Configuration;
using WindowsFormsApp1.Properties;

namespace WindowsFormsApp1
{
    public partial class Kullanıcı : Form
    {
        public Kullanıcı()
        {
            InitializeComponent();
        }
        int timerkontrol = 0;

        public int rolid { get; set; }

        public int bolumid { get; set; }

        private bool isCollabsed = true;

        SqlConnection baglanti = new SqlConnection(ConfigurationManager.ConnectionStrings["myConn"].ConnectionString);
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (timerkontrol == 0)
            {
                flowLayoutPanel1.Width = flowLayoutPanel1.Width - 10;
                if (flowLayoutPanel1.Width == 110)
                {
                    timer1.Stop();
                    timerkontrol = 1;

                }
            }
            else if (timerkontrol == 1)
            {
                flowLayoutPanel1.Width = flowLayoutPanel1.Width + 10;
                if (flowLayoutPanel1.Width == 200)
                {
                    timer1.Stop();
                    timerkontrol = 0;

                    button1.Visible = true;
                    button20.Visible = true;
                    button16.Visible = true;
                    button11.Visible = true;
                    button4.Visible = true;
                    button3.Visible = true;
                    button9.Visible = true;
                    button12.Visible = true;
                    button2.Visible = true;
                    button10.Visible = true;
                    label1.Visible = true;
                }
            }
        }
        private void button6_Click(object sender, EventArgs e)
        {
            if (timerkontrol == 0)
            {
                timer1.Start();
                button16.Visible = false;
                button1.Visible = false;
                button20.Visible = false;
                button16.Visible = false;
                button11.Visible = false;
                button2.Visible = false;
                button3.Visible = false;
                button9.Visible = false;
                button12.Visible = false;
                button4.Visible = false;
                button10.Visible = true;
                label1.Visible = false;
            }
            else if (timerkontrol == 1)
            {
                timer1.Start();
            }
        }
        private void getir()
        {
            listView1.Items.Clear();
            if (rolid == 1)
            {
                SqlCommand komut = new SqlCommand("select Ad,Soyad,Eposta,KullaniciAdi,Sifre,Kullanici.ID,Roller.RolAdi,Bolumler.BolumAdı from Kullanici inner join Roller on Kullanici.Rolid=Roller.Id join Bolumler on Bolumler.BolumID=Kullanici.Bolumid", baglanti);
                if (baglanti.State == ConnectionState.Closed)
                {
                    baglanti.Open();
                }

                SqlDataReader dr = komut.ExecuteReader();
                while (dr.Read())
                {
                    ListViewItem item = new ListViewItem(dr["Ad"].ToString());
                    item.SubItems.Add(dr["Soyad"].ToString());
                    item.SubItems.Add(dr["Eposta"].ToString());
                    item.SubItems.Add(dr["KullaniciAdi"].ToString());
                    item.SubItems.Add(dr["RolAdi"].ToString());
                    item.SubItems.Add(dr["ID"].ToString());
                    item.SubItems.Add(dr["Sifre"].ToString());
                    item.SubItems.Add(dr["BolumAdı"].ToString());

                    listView1.Items.Add(item);
                }
                listView1.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
                dr.Close();
                baglanti.Close();

                baglanti.Open();
                SqlCommand komut2 = new SqlCommand("select * from Bolumler", baglanti);
                SqlDataReader dr2 = komut2.ExecuteReader();
                Dictionary<string, string> dis = new Dictionary<string, string>();

                dis.Add("", "Seçiniz");
                while (dr2.Read())
                {
                    dis.Add(dr2.GetValue(0).ToString(), dr2.GetValue(1).ToString());
                }
                comboBox4.DataSource = new BindingSource(dis, null);
                comboBox4.DisplayMember = "Value";
                comboBox4.ValueMember = "Key";

                comboBox3.DataSource = new BindingSource(dis, null);
                comboBox3.DisplayMember = "Value";
                comboBox3.ValueMember = "Key";
                baglanti.Close();
            }
            // alan şefine göre öğretmen getir
            if (rolid == 2)
            {

                SqlCommand komut3 = new SqlCommand("select Ad,Soyad,Eposta,KullaniciAdi,Sifre,Kullanici.ID,Roller.RolAdi,Bolumler.BolumAdı from " +
                "Kullanici inner join Roller on Kullanici.Rolid = Roller.Id join Bolumler on Bolumler.BolumID = Kullanici.Bolumid " +
               "where Bolumid =@1 and Rolid=@2", baglanti);
                komut3.Parameters.AddWithValue("@1", bolumid);
                komut3.Parameters.AddWithValue("@2", 3);
                baglanti.Open();
                SqlDataReader dr3 = komut3.ExecuteReader();
                while (dr3.Read())
                {
                    ListViewItem item = new ListViewItem(dr3["Ad"].ToString());
                    item.SubItems.Add(dr3["Soyad"].ToString());
                    item.SubItems.Add(dr3["Eposta"].ToString());
                    item.SubItems.Add(dr3["KullaniciAdi"].ToString());
                    item.SubItems.Add(dr3["RolAdi"].ToString());
                    item.SubItems.Add(dr3["ID"].ToString());
                    item.SubItems.Add(dr3["Sifre"].ToString());
                    item.SubItems.Add(dr3["BolumAdı"].ToString());

                    listView1.Items.Add(item);
                }
                dr3.Close();
                listView1.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);

                baglanti.Close();
            }

        }
        private void Kullanıcı_Load(object sender, EventArgs e)
        {
            if (rolid == 2)
            {
                baglanti.Open();
                SqlCommand komut4 = new SqlCommand("select*from Bolumler", baglanti);
                SqlDataReader dr4 = komut4.ExecuteReader();
                Dictionary<string, string> dis3 = new Dictionary<string, string>();

                dis3.Add("", "Seçiniz");
                while (dr4.Read())
                {
                    dis3.Add(dr4.GetValue(0).ToString(), dr4.GetValue(1).ToString());
                }
                dr4.Close();
                comboBox4.DataSource = new BindingSource(dis3, null);
                comboBox4.DisplayMember = "Value";
                comboBox4.ValueMember = "Key";

                comboBox3.DataSource = new BindingSource(dis3, null);
                comboBox3.DisplayMember = "Value";
                comboBox3.ValueMember = "Key";
                baglanti.Close();
            }

            //listView1.Columns.Add("Ad", 100);
            //listView1.Columns.Add("Soyad", 82);
            //listView1.Columns.Add("Eposta", 150);
            //listView1.Columns.Add("Kullanıcı Adı", 95);
            //listView1.Columns.Add("Rol", 80);
            //listView1.Columns.Add("Şifre", 0);
            //listView1.Columns.Add("ID", 0);
            //listView1.Columns.Add("BolumAdı", 130);


            button5.Enabled = false;
            textBox10.Enabled = true;
            textBox7.Enabled = true;
            textBox6.Enabled = true;
            textBox9.Enabled = true;
            textBox8.Enabled = true;
            button10.Enabled = true;
            checkBox2.Enabled = true;


            listView1.CheckBoxes = true;
            listView1.MultiSelect = false;
            listView1.FullRowSelect = true;


            if (rolid == 2)
            {
                button20.Enabled = true;
                button1.Enabled = true;
                button3.Enabled = true;
                button2.Enabled = true;
                button11.Visible = false;
                button16.Enabled = true;
                comboBox2.Enabled = false;
                comboBox4.Enabled = false;
            }
            else if (rolid == 3)
            {
                button20.Enabled = false;
                button1.Enabled = false;
                button3.Enabled = true;
                button2.Enabled = false;
                button11.Enabled = false;
                button9.Enabled = false;
                comboBox2.Enabled = false;
                comboBox4.Enabled = false;
                button16.Enabled = false;
                comboBox1.Enabled = false;
            }
            getir();


            baglanti.Open();
            SqlCommand command = new SqlCommand("SELECT * from Roller", baglanti);
            command.ExecuteNonQuery();
            SqlDataReader commanddr = command.ExecuteReader();
            comboBox2.Items.Add("Seçiniz");
            comboBox1.Items.Add("Seçiniz");
            int sayac = 0;

            while (commanddr.Read())
            {
                if (sayac != 0)
                {
                    comboBox2.Items.Add(commanddr["RolAdi"]);

                }
                sayac++;
                comboBox1.Items.Add(commanddr["RolAdi"]);
            }
            comboBox1.Text = "Seçiniz";
            if (rolid == 1)
            {
                comboBox2.Text = "Seçiniz";
            }
            baglanti.Close();
            if (rolid == 2)
            {
                baglanti.Open();
                comboBox2.Text = "Öğretmen";
                SqlCommand komut = new SqlCommand("select BolumAdı from Bolumler where BolumID='" + bolumid + "'", baglanti);
                SqlDataReader dr = komut.ExecuteReader();
                if (dr.Read())
                {
                    comboBox4.Text = dr[0].ToString();
                }
                dr.Close();
                baglanti.Close();
            }

        }
        private void güncelleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            groupBox2.Visible = true;
        }
        private void label1_MouseHover(object sender, EventArgs e)
        {
        }
        private void pictureBox6_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        private void pictureBox6_Click_1(object sender, EventArgs e)
        {
            DialogResult sondurum = MessageBox.Show("Uygulamayı Kapatmak İstiyor musunuz ?", "Bildirim Penceresi", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (sondurum == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
        }
        private void button1_Click(object sender, EventArgs e)
        {
            KategoriEkle open = new KategoriEkle();
            this.Visible = false;
            open.rolid = rolid;
            open.bolumid = bolumid;
            open.ShowDialog();
            flowLayoutPanel1.Height = button1.Height;
        }

        private void button3_Click(object sender, EventArgs e)
        {
        }
        private void güncelleToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            groupBox2.Visible = true;
        }
        private Point mouse_offset;
        private void panel2_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                Point mousePos = Control.MousePosition;
                mousePos.Offset(mouse_offset.X, mouse_offset.Y);
                this.Location = mousePos;
            }
        }
        private void panel2_MouseDown(object sender, MouseEventArgs e)
        {
            mouse_offset = new Point(-e.X, -e.Y);
        }
        private void button2_Click(object sender, EventArgs e)
        {
            Mekan mek = new Mekan();
            this.Visible = false;
            mek.rolid = rolid;
            mek.bolumid = bolumid;
            mek.ShowDialog();
            flowLayoutPanel1.Height = button2.Height;
        }
        private void listView1_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            label6.Text = "Seçilen Kişi Sayısı: " + listView1.CheckedItems.Count;
            if (listView1.CheckedItems.Count == 0)
            {
                button5.Visible = true;

                button5.Enabled = false;

            }
            else if (listView1.CheckedItems.Count == 1)
            {
                button5.Visible = true;
                button5.Enabled = true;

            }
            else if (listView1.CheckedItems.Count > 1)
            {

                button5.Visible = true;
                button5.Enabled = true;

            }
        }
        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult sondurum;
                if (listView1.CheckedItems.Count == 1)
                {
                    sondurum = MessageBox.Show("Seçilen kullanıcının silinmesini istiyor musunuz ?", "Bildirim Penceresi", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                }
                else
                {
                    sondurum = MessageBox.Show("Seçilen kullanıcıların silinmesini istiyor musunuz ?", "Bildirim Penceresi", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                }
                if (sondurum == DialogResult.Yes)
                {
                    for (int i = 0; i < listView1.CheckedItems.Count; i++)
                    {
                        SqlCommand komut = new SqlCommand("delete from kullanici where ID=@id", baglanti);
                        komut.Parameters.AddWithValue("@id", listView1.CheckedItems[i].SubItems[5].Text);
                        baglanti.Open();
                        komut.ExecuteNonQuery();
                        baglanti.Close();
                    }
                    getir();

                    if (listView1.CheckedItems.Count == 1)
                    {
                        MessageBox.Show("Seçilen kullanıcının hesabı başarıyla silindi.", "Bildirim Penceresi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("Seçilen kullanıcıların hesabı başarıyla silindi.", "Bildirim Penceresi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    textBox1.Clear();
                    textBox2.Clear();
                    textBox3.Clear();
                    textBox4.Clear();
                    textBox5.Clear();

                    comboBox1.Text = "Seçiniz";
                    comboBox3.Text = "Seçiniz";

                    textBox1.Enabled = false;
                    textBox4.Enabled = false;
                    textBox5.Enabled = false;
                    textBox2.Enabled = false;
                    textBox3.Enabled = false;
                    comboBox1.Enabled = false;
                    comboBox3.Enabled = false;
                    checkBox1.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        private void button8_Click(object sender, EventArgs e)
        {
            bool durum2 = false;
            bool durum3 = false;
            if (textBox1.Text == "" || textBox2.Text == "" || textBox3.Text == "" || textBox4.Text == "" || textBox5.Text == "")
            {
                MessageBox.Show("Bütün bilgiler doldurulmalıdır.", "Bildirim Penceresi", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {

                //SqlCommand komut = new SqlCommand("select ID from Kullanıcı where KullaniciAdi='" + textBox1.Text + "'", baglanti);
                if (listView1.Items.Count > 0)
                {
                    if (baglanti.State==ConnectionState.Closed)
                    {
                        baglanti.Open();
                    }
                    SqlCommand komut = new SqlCommand("select ID,Eposta,KullaniciAdi from Kullanici where Eposta='"+ textBox2.Text +"' or KullaniciAdi='"+ textBox1.Text +"'",baglanti);
                    SqlDataReader dr = komut.ExecuteReader();
                    while(dr.Read())
                    {
                        if (dr[0].ToString() != listView1.SelectedItems[0].SubItems[5].Text)
                        {
                            durum2 = true;
                        }
                    }
                    dr.Close();
                    if (textBox1.Text == listView1.SelectedItems[0].SubItems[2].Text &&  textBox2.Text== listView1.SelectedItems[0].SubItems[3].Text && textBox4.Text == listView1.SelectedItems[0].SubItems[0].Text && textBox5.Text == listView1.SelectedItems[0].SubItems[1].Text && textBox3.Text == listView1.SelectedItems[0].SubItems[6].Text)
                    {
                        MessageBox.Show("Bilgileri değiştirmediniz.","Bilgilendirme Penceresi",MessageBoxButtons.OK,MessageBoxIcon.Warning);
                    }
                    else if (!durum2)
                    {
                        if (baglanti.State == ConnectionState.Closed)
                        {
                            baglanti.Open();
                        }
                        int getRoleidd = 0;
                        int getBolumid = 0;
                        SqlCommand command = new SqlCommand("Select * from Roller where RolAdi = '" + comboBox1.Text + "'", baglanti);
                        command.ExecuteNonQuery();
                        SqlDataReader commanddr = command.ExecuteReader();
                        if (commanddr.Read())
                        {
                            getRoleidd = Convert.ToInt32(commanddr["Id"]);
                        }
                        commanddr.Close();
                        baglanti.Close();

                        //değişiklik
                        baglanti.Open();
                        SqlCommand command2 = new SqlCommand("select * from Bolumler where BolumAdı = '" + comboBox3.Text + "'", baglanti);
                        command2.ExecuteNonQuery();
                        SqlDataReader commanddr2 = command2.ExecuteReader();
                        if (commanddr2.Read())
                        {
                            getBolumid = Convert.ToInt32(commanddr2["BolumID"]);
                        }
                        commanddr2.Close();
                        baglanti.Close();

                        for (int i = 0; i < 1; i++)
                        {
                            SqlCommand guncelle = new SqlCommand("update Kullanici set KullaniciAdi=@kullanici,Ad=@adi,Soyad=@soyad,Eposta=@eposta,Sifre=@sifre,Rolid=@rol,Bolumid=@bolumid where ID=@id", baglanti);

                            guncelle.Parameters.AddWithValue("@kullanici", textBox1.Text);
                            guncelle.Parameters.AddWithValue("@eposta", textBox2.Text);
                            guncelle.Parameters.AddWithValue("@sifre", textBox3.Text);
                            guncelle.Parameters.AddWithValue("@adi", textBox4.Text);
                            guncelle.Parameters.AddWithValue("soyad", textBox5.Text);
                            guncelle.Parameters.AddWithValue("@rol", getRoleidd);
                            guncelle.Parameters.AddWithValue("@bolumid", getBolumid);
                            guncelle.Parameters.AddWithValue("@id", id);

                            try
                            {
                                baglanti.Open();
                                guncelle.ExecuteNonQuery();

                                baglanti.Close();
                                button5.Enabled = true;
                                textBox4.Enabled = false;
                                textBox5.Enabled = false;
                                button8.Enabled = false;

                                textBox1.Enabled = false;
                                textBox2.Enabled = false;
                                textBox3.Enabled = false;
                                comboBox1.Enabled = false;
                                comboBox3.Enabled = false;
                                checkBox1.Enabled = false;
                                checkBox2.Enabled = false;
                                comboBox3.Text = "";
                                getir();
                                textBox1.Text = "";
                                textBox2.Text = "";
                                textBox3.Text = "";
                                textBox4.Text = "";
                                textBox5.Text = "";
                                comboBox1.Text = "Seçiniz";

                                //listView1.CheckBoxes = false;

                                MessageBox.Show("Kullanıcı güncellendi.", "Bildirim Penceresi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }

                            catch (Exception ex)
                            {
                                MessageBox.Show("Kayıt Güncellenemedi.", "Bildirim Penceresi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                MessageBox.Show("" + ex);
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("Böyle bir kullanıcı adına veya e-postaya sahip bir kullanıcı zaten var.\rLütfen başka bir e-posta veya kullanıcı adı deneyin.", "Bildirim Penceresi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                    //foreach (ListViewItem kontrol in listView1.Items)
                    //{
                    //    kid = Convert.ToInt32(kontrol.SubItems[5].Text);

                    //    if (textBox1.Text == kontrol.SubItems[3].Text && kullaniciid != kid)
                    //    {
                    //        durum2 = true;
                    //    }
                    //    if (textBox2.Text == kontrol.SubItems[2].Text && kullaniciid != kid)
                    //    {
                    //        durum3 = true;
                    //    }
                    //}
                }
             

            }

        }
        int id = 0;
        private void button9_Click(object sender, EventArgs e)
        {

        }
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
                textBox3.PasswordChar = '\0';
            else if (checkBox1.Checked == false)
                textBox3.PasswordChar = '*';
        }
        private void Panel2_Paint(object sender, PaintEventArgs e)
        {
        }
        private void Label2_Click(object sender, EventArgs e)
        {
        }
        private void CheckBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox2.Checked == true)
                textBox8.PasswordChar = '\0';
            else if (checkBox2.Checked == false)
                textBox8.PasswordChar = '*';
        }
        //Ekle  Butonu  
        private void Getir()
        {
            SqlCommand komut = new SqlCommand("select ID,Ad,Soyad,Eposta,KullaniciAdi,Sifre,Rolid,Bolumid,from Kullanici", baglanti);
            baglanti.Open();
            SqlDataReader dr = komut.ExecuteReader();
            while (dr.Read())
            {
                ListViewItem item = new ListViewItem(dr["Ad,Soyad,Eposta,KullaniciAdi,Sifre,Rolid,Bolumid"].ToString());
                item.SubItems.Add(dr["ID"].ToString());
                listView1.Items.Add(item);
            }
            dr.Close();
            baglanti.Close();
        }

        //Ekleme yapılan yer Yetkili


        private void Button10_Click(object sender, EventArgs e)
        {

            if (baglanti.State == ConnectionState.Closed)
            {
                baglanti.Open();
            }

            int getRoleid = rolid;
            int getBolumid = bolumid;

            SqlCommand command = new SqlCommand("Select * from Roller where RolAdi = '" + comboBox2.Text + "'", baglanti);
            SqlDataReader commanddr = command.ExecuteReader();

            if (commanddr.Read())
            {
                getRoleid = Convert.ToInt32(commanddr["Id"]);
            }
            commanddr.Close();
            baglanti.Close();
            if (rolid == 2 || rolid == 1)
            {
                baglanti.Open();
                SqlCommand komut3 = new SqlCommand("select ID,Eposta,KullaniciAdi from Kullanici where KullaniciAdi='" + textBox10.Text + "' or Eposta='" + textBox9.Text + "'", baglanti);
                SqlDataReader dr3 = komut3.ExecuteReader();
                if (!dr3.Read())
                {
                    dr3.Close();
                    if (textBox10.Text != "" && textBox7.Text != "" && textBox6.Text != "" && textBox9.Text != "" && textBox8.Text != "" && comboBox2.SelectedIndex != 0 && comboBox4.SelectedIndex != 0)
                    {
                        string sorgu = "insert into Kullanici(Ad,Soyad,Eposta,KullaniciAdi,Sifre,Rolid,Bolumid)values(@adi,@soyad,@eposta,@kullaniciad,@sifre,@rolid,@bolumid)";

                        SqlCommand komut = new SqlCommand(sorgu, baglanti);

                        komut.Parameters.AddWithValue("@kullaniciad", textBox10.Text);
                        komut.Parameters.AddWithValue("@adi", textBox7.Text);
                        komut.Parameters.AddWithValue("@soyad", textBox6.Text);
                        komut.Parameters.AddWithValue("@eposta", textBox9.Text);
                        komut.Parameters.AddWithValue("@sifre", textBox8.Text);

                        if (rolid == 1)
                        {
                            komut.Parameters.AddWithValue("@rolid", getRoleid);
                            komut.Parameters.AddWithValue("@bolumid", comboBox4.SelectedValue);
                        }
                        else if (rolid == 2)
                        {
                            komut.Parameters.AddWithValue("@rolid", 3);
                            komut.Parameters.AddWithValue("@bolumid", getBolumid);
                        }

                        try
                        {

                            komut.ExecuteNonQuery();

                            baglanti.Close();
                            listView1.Items.Clear();

                            getir();
                            MessageBox.Show("Kullanıcı eklenmiştir", "Bildirim Penceresi", MessageBoxButtons.OK, MessageBoxIcon.Information);

                            textBox10.Clear();
                            textBox7.Clear();
                            textBox6.Clear();
                            textBox9.Clear();
                            textBox8.Clear();
                            if (rolid == 1)
                            {
                                comboBox2.Text = "Seçiniz";
                            }
                        }
                        catch (Exception hata)
                        {
                            baglanti.Close();
                            MessageBox.Show("Kaydınız Veri Tabanına eklenmemiştir " + hata.Message);
                        }

                    }
                    else
                    {
                        MessageBox.Show("Lütfen bilgileri eksik girmeyiniz.", "Bildirim Penceresi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Böyle bir kullanıcı adına veya e-postaya sahip bir kullanıcı zaten var.\rLütfen başka bir e-posta ve kullanıcı adı deneyin.", "Bildirim Penceresi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }






            }
        }
        private void GroupBox2_Enter(object sender, EventArgs e)
        {
        }
        private void Label6_Click(object sender, EventArgs e)
        {
        }
        private void GroupBox3_Enter(object sender, EventArgs e)
        {
        }
        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        private void Button11_Click(object sender, EventArgs e)
        {
            Bolumler sa = new Bolumler();
            this.Visible = false;
            sa.rolid = rolid;
            sa.bolumid = bolumid;
            sa.ShowDialog();
            flowLayoutPanel1.Height = button11.Height;
        }
        private void ComboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {
        }
        int kullaniciid = 0;
        private void ListView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count > 0)
            {
                for (int i = 0; i < listView1.SelectedItems.Count; i++)
                {

                    textBox1.Text = listView1.SelectedItems[i].SubItems[3].Text;
                    textBox2.Text = listView1.SelectedItems[i].SubItems[2].Text;
                    textBox3.Text = listView1.SelectedItems[i].SubItems[6].Text;
                    textBox4.Text = listView1.SelectedItems[i].SubItems[0].Text;
                    textBox5.Text = listView1.SelectedItems[i].SubItems[1].Text;

                    comboBox1.Text = listView1.SelectedItems[i].SubItems[4].Text;
                    comboBox3.Text = listView1.SelectedItems[i].SubItems[7].Text;
                    id = Convert.ToInt32(listView1.SelectedItems[i].SubItems[5].Text);

                    button5.Enabled = false;

                    button8.Enabled = true;
                    textBox1.Enabled = true;
                    textBox2.Enabled = true;
                    textBox3.Enabled = true;
                    textBox4.Enabled = true;
                    textBox5.Enabled = true;
                    checkBox1.Enabled = true;
                    comboBox1.Items.Remove("Sistem Yöneticisi");
                    if (rolid == 1)
                    {
                        comboBox1.Enabled = true;
                        comboBox3.Enabled = true;
                    }
                    else if (rolid == 2)
                    {
                        comboBox1.Enabled = false;
                        comboBox3.Enabled = false;
                    }
                }
                if (baglanti.State == ConnectionState.Closed)
                {
                    baglanti.Open();
                }

                SqlCommand komut = new SqlCommand("select ID from Kullanici where KullaniciAdi='" + textBox1.Text + "'", baglanti);
                SqlDataReader dr = komut.ExecuteReader();
                if (dr.Read())
                {
                    kullaniciid = (int)dr[0];
                }
                baglanti.Close();
                if (listView1.CheckedItems.Count > 0)
                {
                    button5.Enabled = true;
                }
                else
                {
                    button5.Enabled = false;
                }
            }
            else
            {
                button8.Enabled = false;
                textBox1.Clear();
                textBox2.Clear();
                textBox3.Clear();
                textBox4.Clear();
                textBox5.Clear();
                checkBox1.Checked = false;




                button8.Enabled = false;
                textBox1.Enabled = false;
                textBox2.Enabled = false;
                textBox3.Enabled = false;
                textBox4.Enabled = false;
                textBox5.Enabled = false;
                checkBox1.Enabled = false;
                comboBox1.Enabled = false;
                comboBox3.Enabled = false;
                comboBox1.Text = "Seçiniz";
                comboBox3.Text = "Seçiniz";

            }

        }

        private void Button9_Click_1(object sender, EventArgs e)
        {
            DemirbasEkle open = new DemirbasEkle();
            this.Visible = false;

            open.rolid = rolid;
            open.bolumid = bolumid;
            open.ShowDialog();
        }

        private void Button3_Click_1(object sender, EventArgs e)
        {
            DemirbasAyarlar ayar = new DemirbasAyarlar();
            this.Hide();
            ayar.rolid = rolid;
            ayar.bolumid = bolumid;
            ayar.ShowDialog();
        }

        private void Timer2_Tick(object sender, EventArgs e)
        {
            if (isCollabsed)
            {
                button12.Image = Resources.up_arrow;
                panelDropDown.Height += 10;
                if (panelDropDown.Size == panelDropDown.MaximumSize)
                {
                    timer2.Stop();
                    isCollabsed = false;
                }
            }
            else
            {
                button12.Image = Resources.angle_arrow_down;
                panelDropDown.Height -= 10;
                if (panelDropDown.Size == panelDropDown.MinimumSize)
                {
                    timer2.Stop();
                    isCollabsed = true;
                }
            }
        }

        private void Button12_Click(object sender, EventArgs e)
        {
            timer2.Start();
        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox7_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBox1.Items.Add("Seçiniz");
        }

        private void ComboBox1_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex <= 0 || comboBox3.SelectedIndex <= 0)
            {
                button8.Enabled = false;
            }
            else
            {
                button8.Enabled = true;
            }
        }

        private void ComboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex <= 0 || comboBox3.SelectedIndex <= 0)
            {
                button8.Enabled = false;
            }
            else
            {
                button8.Enabled = true;
            }
        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {
            DialogResult sondurum = MessageBox.Show("Oturumu kapatmak istiyor musunuz ?", "Bildirim Penceresi", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (sondurum == DialogResult.Yes)
            {
                Giris open = new Giris();
                this.Visible = false;
                open.ShowDialog();
            }
        }

        private void listView1_ColumnWidthChanged(object sender, ColumnWidthChangedEventArgs e)
        {

            if (listView1.Columns[5].Width > 0)
            {
                listView1.Columns[5].Width = 0;

            }
            if (listView1.Columns[6].Width > 0)
            {
                listView1.Columns[6].Width = 0;

            }


        }

        private void button16_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Daha yapılmadı");
        }

        private void button20_Click(object sender, EventArgs e)
        {
            Anasayfa open = new Anasayfa();
            this.Visible = false;
            open.bolumid = bolumid;
            open.rolid = rolid;
            open.ShowDialog();
        }
    }
}