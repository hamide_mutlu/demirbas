﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Configuration;
using WindowsFormsApp1.Properties;


using System.Diagnostics;
using System.IO;

namespace WindowsFormsApp1
{
    public partial class Anasayfa : Form
    {
        public Anasayfa()
        {
            InitializeComponent();
        }
        int timerkontrol = 0;

        private bool isCollabsed = true;

        SqlConnection baglanti = new SqlConnection(ConfigurationManager.ConnectionStrings["myConn"].ConnectionString);
        public int rolid { get; set; }
        public int bolumid { get; set; }


        private void pictureBox6_Click(object sender, EventArgs e)
        {
            DialogResult sondurum = MessageBox.Show("Uygulamayı Kapatmak İstiyor musunuz ?", "Bilgilendirme Penceresi", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (sondurum == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            serialPort1.Close();
            KategoriEkle open = new KategoriEkle();
            this.Visible = false;
            open.rolid = rolid;
            open.bolumid = bolumid;
            open.ShowDialog();
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            //serialPort1.Close();
            //DemirbasEkle open = new DemirbasEkle();
            //this.Visible = false;
            //open.rolid = rolid;
            //open.ShowDialog();
            timer2.Start();

        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            serialPort1.Close();
            Mekan open = new Mekan();
            this.Visible = false;
            open.rolid = rolid;
            open.bolumid = bolumid;
            open.ShowDialog();

        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            serialPort1.Close();
            Kullanıcı open = new Kullanıcı();
            this.Visible = false;
            open.bolumid = bolumid;
            open.rolid = rolid;
            open.ShowDialog();
        }
        //string[] portlar = SerialPort.GetPortNames();
        //private string rxString;
        //string portname = string.Empty;

        private void Form1_Load(object sender, EventArgs e)
        {

            SqlCommand sorgu3 = new SqlCommand("select Ad  from Kullanici where Rolid=@rolid", baglanti);
            sorgu3.Parameters.AddWithValue("@rolid", rolid);

            baglanti.Open();
            string sonuc3 = (string)sorgu3.ExecuteScalar();
            baglanti.Close();

            if (rolid == 2)
            {
                button1.Enabled = true;
                button2.Enabled = true;
                button3.Enabled = true;
                button4.Enabled = true;
                button11.Enabled = true;
                button5.Visible = false;

            }
            else if (rolid == 3)
            {
                button12.Visible = true;
                button1.Enabled = false;
                button2.Enabled = false;
                button4.Enabled = false;
                button5.Enabled = false;
                button10.Enabled = false;
                button3.Enabled = true;
                button11.Enabled = false;

            }
            //foreach (string port in portlar)
            //{
            //    portname = port;
            //}
            //serialPort1.PortName = portname;
            //serialPort1.Open();
        }
        private void button6_Click(object sender, EventArgs e)
        {
            if (timerkontrol == 0)
            {
                timer1.Start();
                button1.Visible = false;
                button5.Visible = false;
                button2.Visible = false;
                button3.Visible = false;
                button4.Visible = false;
                button10.Visible = false;
                button11.Visible = false;
                button12.Visible = false;
                button8.Visible = false;
                label1.Visible = false;
                button11.Visible = false;
            }
            else if (timerkontrol == 1)
            {
                timer1.Start();

            }
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (timerkontrol == 0)
            {
                flowLayoutPanel1.Width = flowLayoutPanel1.Width - 10;
                if (flowLayoutPanel1.Width == 110)
                {
                    timer1.Stop();
                    timerkontrol = 1;
                }
            }
            else if (timerkontrol == 1)
            {
                flowLayoutPanel1.Width = flowLayoutPanel1.Width + 10;
                if (flowLayoutPanel1.Width == 200)
                {
                    timer1.Stop();
                    timerkontrol = 0;
                    button1.Visible = true;
                    button5.Visible = true;
                    button4.Visible = true;
                    button3.Visible = true;
                    button2.Visible = true;
                    button10.Visible = true;
                    button11.Visible = true;
                    button12.Visible = true;
                    button8.Visible = true;
                    label1.Visible = true;
                }
            }
        }
        private Point mouse_offset;
        private void panel2_MouseDown(object sender, MouseEventArgs e)
        {
            mouse_offset = new Point(-e.X, -e.Y);
        }

        private void panel2_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                Point mousePos = Control.MousePosition;
                mousePos.Offset(mouse_offset.X, mouse_offset.Y);
                this.Location = mousePos;
            }
        }

        private void serialPort1_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            //rxString = serialPort1.ReadExisting();
            //serialPort1.Close();
            //Anasayfa ana = new Anasayfa();
            //DemirbasAyarlar ayar = new DemirbasAyarlar();
            //ana.Hide();
            //ayar.Show();
            //ayar.verigeldi(rxString);
        }

        private void pictureBox7_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void Button5_Click(object sender, EventArgs e)
        {
            serialPort1.Close();
            Bolumler open = new Bolumler();
            this.Visible = false;
            open.rolid = rolid;
            open.bolumid = bolumid;
            open.ShowDialog();
        }

        private void Button7_Click(object sender, EventArgs e)
        {

        }

        private void button9_Click(object sender, EventArgs e)
        {
            DemirbasAyarlar ayar = new DemirbasAyarlar();
            this.Visible = false;
            ayar.rolid = rolid;
            ayar.bolumid = bolumid;
            ayar.ShowDialog();

        }

        private void Timer2_Tick(object sender, EventArgs e)
        {
            if (isCollabsed)
            {
                button3.Image = Resources.up_arrow;
                panelDropDown.Height += 10;
                if (panelDropDown.Size == panelDropDown.MaximumSize)
                {
                    timer2.Stop();
                    isCollabsed = false;
                }
            }
            else
            {
                button3.Image = Resources.angle_arrow_down;
                panelDropDown.Height -= 10;
                if (panelDropDown.Size == panelDropDown.MinimumSize)
                {
                    timer2.Stop();
                    isCollabsed = true;
                }
            }
        }

        private void Button10_Click(object sender, EventArgs e)
        {
            DemirbasEkle open = new DemirbasEkle();
            this.Visible = false;
            open.rolid = rolid;
            open.bolumid = bolumid;
            open.ShowDialog();
        }

        private void Button8_Click(object sender, EventArgs e)
        {
            DemirbasAyarlar ayar = new DemirbasAyarlar();
            this.Hide();
            ayar.rolid = rolid;
            ayar.bolumid = bolumid;
            ayar.ShowDialog();
        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {
            DialogResult sondurum = MessageBox.Show("Kullanıcı değiştirmek istiyor musunuz ?", "Bilgilendirme Penceresi", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (sondurum == DialogResult.Yes)
            {
                Giris open = new Giris();
                this.Visible = false;
                open.ShowDialog();
            }

        }

        private void LinkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ///System.Diagnostics.Process.Start(Dokumantasyon.html);
            ///
            if (rolid == 1)
            {
                string filepath = Path.Combine(Environment.CurrentDirectory, "Dokumantasyon.html");
                Process.Start(filepath);
            }
            else if (rolid == 2)
            {
                System.Diagnostics.Process.Start("Dokumantasyon(Bölüm_Sorumlusu.html"); 
            }
            else if (rolid == 3)
            {
                System.Diagnostics.Process.Start("Dokumantasyon(Öğretmen).html");
            }

        }

        private void button11_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Daha yapılmadı");
        }

        private void label11_Click(object sender, EventArgs e)
        {

        }
    }
}
