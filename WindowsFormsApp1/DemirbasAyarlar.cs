﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Configuration;
using WindowsFormsApp1.Properties;

namespace WindowsFormsApp1
{
    public partial class DemirbasAyarlar : Form
    {
        public DemirbasAyarlar()
        {
            InitializeComponent();
        }
        private Point mouse_offset;

        public int rolid { get; set; }

        public int bolumid { get; set; }

        private bool isCollabsed = true;

        string[] portlar = SerialPort.GetPortNames();
        private string rxString;
        private void combogetir()
        {
            baglanti.Open();
            SqlCommand komut2 = new SqlCommand("select*from Bolumler", baglanti);
            SqlDataAdapter da = new SqlDataAdapter(komut2);
            SqlDataReader dr2 = komut2.ExecuteReader();
            Dictionary<string, string> dis = new Dictionary<string, string>();

            dis.Add("", "Seçiniz");
            while (dr2.Read())
            {
                dis.Add(dr2.GetValue(0).ToString(), dr2.GetValue(1).ToString());
            }
            comboBox4.DataSource = new BindingSource(dis, null);
            comboBox4.DisplayMember = "Value";
            comboBox4.ValueMember = "Key";
            dr2.Close();
            baglanti.Close();
        }
        SqlConnection baglanti = new SqlConnection(ConfigurationManager.ConnectionStrings["myConn"].ConnectionString);
        List<string> liste = new List<string>();

        //string[] portlar = SerialPort.GetPortNames();
        //private string rxString;
        //string portname = string.Empty;
        private void DemirbasAyarlar_Load(object sender, EventArgs e)
        {


            if (rolid == 1)
            {
                button8.Enabled = false;
                label6.Visible = false;
                combogetir();
            }
            if (rolid == 3)
            {
                button8.Enabled = false;
                listView1.CheckBoxes = false;
                groupBox3.Visible = false;
                label11.Visible = false;
                button11.Visible = false;
                pictureBox1.Visible = false;
                button1.Enabled = false;
                button20.Visible = true;
                button16.Enabled = false;
                button2.Enabled = false;
                button4.Enabled = false;
                button13.Enabled = false;
            }
            comboBox1.Items.Add("Seçiniz");
            baglanti.Open();

            baglanti.Close();
            comboBox1.Text = "Seçiniz";


            SqlCommand sorgu4 = new SqlCommand("select Ad  from Kullanici where Rolid = @rolid", baglanti);
            sorgu4.Parameters.AddWithValue("@rolid", rolid);

            baglanti.Open();
            string sonuc3 = (string)sorgu4.ExecuteScalar();
            baglanti.Close();
            if (rolid == 2 || rolid == 3)
            {
                button8.Enabled = false;
                comboBox4.Visible = false;
                comboBox1.Enabled = true;
                label1.Text = "Bölümünüz :";

                baglanti.Open();
                SqlCommand bolumad = new SqlCommand("select BolumAdı from Bolumler as b " +
                    "inner join Kullanici as k on k.Bolumid=b.BolumID where BolumID='" + bolumid + "'", baglanti);

                SqlDataReader dr = bolumad.ExecuteReader();
                if (dr.Read())
                {
                    label6.Text = dr.GetValue(0).ToString();
                }
                dr.Close();
                baglanti.Close();

                baglanti.Open();

                SqlCommand komut3 = new SqlCommand("select ID,kategoriAd from Kategori where BolumID =" + bolumid, baglanti);
                SqlDataReader dr3 = komut3.ExecuteReader();
                Dictionary<string, string> dis2 = new Dictionary<string, string>();

                dis2.Add("", "Seçiniz");
                while (dr3.Read())
                {
                    dis2.Add(dr3.GetValue(0).ToString(), dr3.GetValue(1).ToString());
                }
                comboBox1.SelectedIndexChanged -= comboBox1_SelectedIndexChanged;
                comboBox1.DataSource = new BindingSource(dis2, null);
                comboBox1.DisplayMember = "Value";
                comboBox1.ValueMember = "Key";
                comboBox1.Text = "";
                comboBox1.SelectedIndexChanged += comboBox1_SelectedIndexChanged;
                dr3.Close();

                baglanti.Close();
            }
            if (rolid == 3)
            {
                button16.Enabled = false;
                button20.Enabled = true;
                button1.Enabled = false;
                button2.Enabled = false;
                button4.Enabled = false;
                button13.Enabled = false;
                button14.Enabled = false;
            }



            //foreach (string port in portlar)
            //{
            //    portname = port;
            //}
            //serialPort1.PortName = portname;
            //serialPort1.Open();
        }
        //private void serialPort2_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        //{
        //    rxString = serialPort1.ReadExisting();
        //    this.Invoke(new EventHandler(displayText));
        //}
        //private void displayText(object o, EventArgs e)
        //{
        //    listView1.Items.Clear();
        //    listView1.Columns.Clear();

        //    if (rxString.Length < 8 || rxString.Length > 9)
        //    {
        //        textBox1.Text = "";
        //        comboBox1.Text = "Seçiniz";
        //        MessageBox.Show("Böyle bir kayıt bulunmamaktadır.", "Bilgilendirme Penceresi", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        //    }
        //    else
        //    {
        //        textBox1.Text = rxString.Substring(0, 8);
        //        string result = textBox1.Text.Substring(0, 3);
        //        SqlCommand sorgu = new SqlCommand("select tabloadi from kodlar where ad='" + result + "'", baglanti);
        //        baglanti.Open();
        //        string ad = (string)sorgu.ExecuteScalar();
        //        baglanti.Close();
        //        if (ad == null)
        //        {
        //            comboBox1.Text = "Seçiniz";
        //            MessageBox.Show("Böyle bir kayıt bulunmamaktadır.", "Bilgilendirme Penceresi", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        //            textBox1.Text = "";
        //        }
        //        else
        //        {
        //            if (comboBox1.Text == ad)
        //                comboBox1_SelectedIndexChanged(o, e);
        //            else
        //                comboBox1.Text = ad;
        //            button5.Enabled = true;
        //            listView1.Items[0].Selected = true;
        //        }
        //    }
        //}

        private void button9_Click(object sender, EventArgs e)
        {

            if (serialPort1.IsOpen == false)
            {
                foreach (string port in portlar)
                {
                    comboBox3.Items.Add(port);
                }
                comboBox3.SelectedIndex = 0;
                serialPort1.PortName = comboBox3.Text;
                serialPort1.Open();
            }
        }


        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            label11.Text = "Seçilen Veri Sayısı: 0";
            listView1.Items.Clear();
            listView2.Items.Clear();
            textBox2.Enabled = false;
            textBox1.Clear();
            textBox2.Clear();
            button10.Enabled = false;
            button11.Enabled = false;
            label7.Text = "Ürün Adı";

            /////listview 1'e ürün adlarını listeletme
            SqlCommand kontrol = new SqlCommand("select UrunID,ürünkodu from Kategori as k " +
                "inner join Urunler as u on u.kategoriID = k.ID where k.ID='" + comboBox1.SelectedValue + "'", baglanti);
            baglanti.Open();

            SqlDataReader dr4 = kontrol.ExecuteReader();
            while (dr4.Read())
            {
                ListViewItem item = new ListViewItem(dr4[1].ToString());
                item.SubItems.Add(dr4[0].ToString());
                listView1.Items.Add(item);
            }
            //

            //////mekan combobox'ına mekanları listeletme
            baglanti.Close();
            SqlCommand mekan = new SqlCommand();
            baglanti.Open();
            if (rolid == 1)
            {
                mekan = new SqlCommand("select * from Mekan where BolumID='" + comboBox4.SelectedValue + "' order by Yer ASC", baglanti);
            }
            else if (rolid == 2 || rolid == 3)
            {
                mekan = new SqlCommand("select * from Mekan where BolumID='" + bolumid + "' order by Yer ASC", baglanti);
            }
            SqlDataReader dr = mekan.ExecuteReader();
            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("", "Seçiniz");
            while (dr.Read())
            {
                dic.Add(dr.GetValue(0).ToString(), dr.GetValue(1).ToString());
            }
            comboBox2.DataSource = new BindingSource(dic, null);
            comboBox2.DisplayMember = "Value";
            comboBox2.ValueMember = "Key";
            comboBox2.Text = "";
            dr.Close();
            baglanti.Close();
            //
            if (comboBox1.Text != "Seçiniz")
            {

            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (timerkontrol == 0)
            {
                timer1.Start();


                button16.Visible = false;
                button20.Visible = false;
                button1.Visible = false;
                button3.Visible = false;
                button2.Visible = false;
                button4.Visible = false;
                button13.Visible = false;
                button14.Visible = false;
                button15.Visible = false;
                label5.Visible = false;
            }
            else if (timerkontrol == 1)
            {
                timer1.Start();

            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            serialPort1.Close();
            Anasayfa sa = new Anasayfa();
            this.Visible = false;

            sa.rolid = rolid;
            sa.bolumid = bolumid;
            sa.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            serialPort1.Close();
            KategoriEkle sa = new KategoriEkle();
            this.Visible = false;
            sa.rolid = rolid;
            sa.bolumid = bolumid;
            sa.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //serialPort1.Close();
            DemirbasEkle sa = new DemirbasEkle();
            this.Visible = false;
            sa.rolid = rolid;
            sa.bolumid = bolumid;
            sa.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //serialPort1.Close();
            Mekan sa = new Mekan();
            this.Visible = false;
            sa.rolid = rolid;
            sa.bolumid = bolumid;
            sa.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //serialPort1.Close();
            Kullanıcı sa = new Kullanıcı();
            this.Visible = false;
            sa.rolid = rolid;
            sa.bolumid = bolumid;
            sa.ShowDialog();
        }

        private void panel2_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                Point mousePos = Control.MousePosition;
                mousePos.Offset(mouse_offset.X, mouse_offset.Y);
                this.Location = mousePos;
            }
        }

        private void panel2_MouseDown(object sender, MouseEventArgs e)
        {
            mouse_offset = new Point(-e.X, -e.Y);
        }

        private void button5_Click(object sender, EventArgs e)
        {//temizle butonu
            textBox1.Text = "";
            listView1.Items.Clear();
            comboBox1.Text = "Seçiniz";
            comboBox2.Enabled = false;
            button8.Enabled = false;
            label7.Text = "Ürün Adı";
            textBox2.Clear();
        }

        private void listView1_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if (listView1.CheckedItems.Count == 0)
            {
                button11.Enabled = false;
            }
            else
            {
                button11.Enabled = true;
            }
            label11.Text = "Seçilen Veri Sayısı: " + listView1.CheckedItems.Count;

        }



        private void listView1_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {

        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            DialogResult sondurum = MessageBox.Show("Uygulamayı Kapatmak İstiyormusunuz ?", "Bilgilendirme Penceresi", MessageBoxButtons.YesNo);
            if (sondurum == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void button11_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult sondurum;
                if (listView1.CheckedItems.Count == 1)
                {
                    sondurum = MessageBox.Show("Seçilen ürünün silinmesini istiyor musunuz ?", "Bilgilendirme Penceresi", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                }
                else
                {
                    sondurum = MessageBox.Show("Seçilen ürünlerin silinmesini istiyor musunuz ?", "Bilgilendirme Penceresi", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                }
                if (sondurum == DialogResult.Yes)
                {
                    for (int i = 0; i < listView1.CheckedItems.Count; i++)
                    {
                        SqlCommand komut1 = new SqlCommand("delete from Urün_Ozellik where urunID = '" + listView1.CheckedItems[i].SubItems[1].Text + "'", baglanti);
                        baglanti.Open();
                        komut1.ExecuteNonQuery();
                        baglanti.Close();


                        SqlCommand komut = new SqlCommand("delete from Urunler where UrunID = '" + listView1.CheckedItems[i].SubItems[1].Text + "'", baglanti);
                        baglanti.Open();
                        komut.ExecuteNonQuery();
                        baglanti.Close();
                    }
                    MessageBox.Show("Seçilen ürün başarıyla silindi.", "Bilgilendirme Penceresi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            button11.Enabled = false;
            label11.Text = "Seçilen Veri Sayısı: 0 ";
            listView1.Items.Clear();
            listView2.Items.Clear();
            SqlCommand kontrol = new SqlCommand("select UrunID,ürünkodu from Kategori as k " +
               "inner join Urunler as u on u.kategoriID = k.ID where k.ID='" + comboBox1.SelectedValue + "'", baglanti);
            baglanti.Open();

            SqlDataReader dr4 = kontrol.ExecuteReader();
            while (dr4.Read())
            {
                ListViewItem item = new ListViewItem(dr4[1].ToString());
                item.SubItems.Add(dr4[0].ToString());
                listView1.Items.Add(item);
            }
            baglanti.Close();

        }
        private void button10_Click(object sender, EventArgs e)
        {
            if (textBox2.Text != listView2.SelectedItems[0].SubItems[3].Text)
            {
                if (listView2.Items.Count > 0)
                {
                    SqlCommand komut = new SqlCommand("update Urün_Ozellik set ozellikDegeri=@deger where ozellikID=@ID and UrunID = @uID", baglanti);
                    komut.Parameters.AddWithValue("@deger", textBox2.Text);
                    komut.Parameters.AddWithValue("@ID", listView2.SelectedItems[0].SubItems[0].Text);
                    komut.Parameters.AddWithValue("@uID", listView1.SelectedItems[0].SubItems[1].Text);

                    try
                    {
                        baglanti.Open();
                        komut.ExecuteNonQuery();
                        baglanti.Close();
                        MessageBox.Show("Güncelleme işlemi yapıldı.", "Bilgilendirme Penceresi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (Exception asd)
                    {
                        MessageBox.Show("Güncelleme işlemi yapılamadı." + asd);
                        throw;
                    }
                    ///////////////listview yenilemek için
                    if (listView1.SelectedItems.Count > 0)
                    {

                        listView2.Items.Clear();
                        SqlCommand kontrol = new SqlCommand("select o.ID,urunID,o.ozellikAdı,ozellikDegeri from Ozellik as o " +
                            "inner join Urün_Ozellik as u on o.ID=u.ozellikID where urunID='" + listView1.SelectedItems[0].SubItems[1].Text + "'", baglanti);
                        baglanti.Open();

                        SqlDataReader dr4 = kontrol.ExecuteReader();
                        while (dr4.Read())
                        {
                            ListViewItem item = new ListViewItem(dr4[0].ToString());
                            item.SubItems.Add(dr4[1].ToString());
                            item.SubItems.Add(dr4[2].ToString());
                            item.SubItems.Add(dr4[3].ToString());
                            listView2.Items.Add(item);
                        }
                        baglanti.Close();
                        button10.Enabled = false;
                        textBox2.Enabled = false;


                        //label11.Text = "Seçilen Veri Sayısı :" + listView1.SelectedItems.Count.ToString();


                    }
                    /////////////////////////////////
                    textBox1.Clear();
                    textBox2.Clear();

                }

            }
            else
            {
                MessageBox.Show("Ürünün değerini değiştirmediniz.", "Bilgilendirme Penceresi", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        private void button12_Click(object sender, EventArgs e)
        {

        }

        private void button8_Click(object sender, EventArgs e)
        {

        }

        private void GroupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void PictureBox7_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        private void PictureBox10_Click(object sender, EventArgs e)
        {
            DialogResult sondurum = MessageBox.Show("Uygulamayı Kapatmak İstiyor musunuz ?", "Bilgilendirme Penceresi", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (sondurum == DialogResult.Yes)
            {
                Application.Exit();
            }
        }
        private void ListView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count > 0)
            {
                button8.Enabled = true;
                comboBox2.Enabled = true;
                button8.Enabled = true;
                label7.Text = listView1.SelectedItems[0].SubItems[0].Text;
                textBox2.Clear();
                textBox2.Enabled = false;
                button10.Enabled = false;
                listView2.Items.Clear();
                SqlCommand kontrol = new SqlCommand("select o.ID,urunID,o.ozellikAdı,ozellikDegeri from Ozellik as o " +
                    "inner join Urün_Ozellik as u on o.ID=u.ozellikID where urunID='" + listView1.SelectedItems[0].SubItems[1].Text + "'", baglanti);
                baglanti.Open();

                SqlDataReader dr4 = kontrol.ExecuteReader();
                while (dr4.Read())
                {
                    ListViewItem item = new ListViewItem(dr4[0].ToString());
                    item.SubItems.Add(dr4[1].ToString());
                    item.SubItems.Add(dr4[2].ToString());
                    item.SubItems.Add(dr4[3].ToString());
                    listView2.Items.Add(item);
                }
                baglanti.Close();

                baglanti.Open();
                SqlCommand mekan1 = new SqlCommand("select m.Yer from Urunler as u inner join Mekan as m on u.mekanID=m.ID where UrunID='" + listView1.SelectedItems[0].SubItems[1].Text + "'", baglanti);
                SqlDataReader dr = mekan1.ExecuteReader();
                if (dr.Read())
                {
                    comboBox2.Text = dr[0].ToString();
                }
                dr.Close();
                baglanti.Close();

            }
            else
            {
                textBox2.Clear();
                button10.Enabled = false;
                textBox2.Enabled = false;
                listView2.Items.Clear();
                comboBox2.Text = "Seçiniz";
                comboBox2.Enabled = false;
                button8.Enabled = false;
                label7.Text = "Ürün Adı";
            }
        }
        private void Button13_Click(object sender, EventArgs e)
        {
            Bolumler sa = new Bolumler();
            this.Visible = false;
            sa.rolid = rolid;
            sa.bolumid = bolumid;

            sa.ShowDialog();
        }
        int timerkontrol = 0;
        private void Timer1_Tick(object sender, EventArgs e)
        {
            if (timerkontrol == 0)
            {
                flowLayoutPanel1.Width = flowLayoutPanel1.Width - 10;
                if (flowLayoutPanel1.Width == 110)
                {
                    timer1.Stop();
                    timerkontrol = 1;
                }
            }
            else if (timerkontrol == 1)
            {
                flowLayoutPanel1.Width = flowLayoutPanel1.Width + 10;
                if (flowLayoutPanel1.Width == 200)
                {
                    timer1.Stop();
                    timerkontrol = 0;
                    button16.Visible = true;
                    button20.Visible = true;
                    button1.Visible = true;
                    button3.Visible = true;
                    button2.Visible = true;
                    button4.Visible = true;
                    button13.Visible = true;
                    button14.Visible = true;
                    button15.Visible = true;
                    label5.Visible = true;
                }
            }
        }

        private void Button15_Click(object sender, EventArgs e)
        {
            timer2.Start();
        }

        private void Timer2_Tick(object sender, EventArgs e)
        {
            if (isCollabsed)
            {
                button15.Image = Resources.up_arrow;
                panelDropDown.Height += 10;
                if (panelDropDown.Size == panelDropDown.MaximumSize)
                {
                    timer2.Stop();
                    isCollabsed = false;
                }
            }
            else
            {
                button15.Image = Resources.angle_arrow_down;
                panelDropDown.Height -= 10;
                if (panelDropDown.Size == panelDropDown.MinimumSize)
                {
                    timer2.Stop();
                    isCollabsed = true;
                }
            }
        }

        private void Button14_Click(object sender, EventArgs e)
        {
            DemirbasEkle open = new DemirbasEkle();
            this.Visible = false;

            open.rolid = rolid;
            open.bolumid = bolumid;
            open.ShowDialog();
        }

        private void ComboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            listView2.Items.Clear();
            textBox1.Clear();
            textBox2.Clear();
            comboBox1.Enabled = false;
            comboBox1.Text = "Seçiniz";
            button11.Enabled = false;
            label11.Text = "Seçilen Veri Sayısı: 0";
            if (comboBox4.SelectedIndex > 0)
            {
                comboBox1.Enabled = true;
                if (baglanti.State==ConnectionState.Closed)
                {
                    baglanti.Open();
                }

                SqlCommand komut3 = new SqlCommand("select ID,kategoriAd from Kategori where BolumID =" + comboBox4.SelectedValue, baglanti);
                SqlDataAdapter da2 = new SqlDataAdapter(komut3);
                SqlDataReader dr3 = komut3.ExecuteReader();
                Dictionary<string, string> dis2 = new Dictionary<string, string>();

                dis2.Add("", "Seçiniz");
                while (dr3.Read())
                {
                    dis2.Add(dr3.GetValue(0).ToString(), dr3.GetValue(1).ToString());
                }
                comboBox1.SelectedIndexChanged -= comboBox1_SelectedIndexChanged;
                comboBox1.DataSource = new BindingSource(dis2, null);
                comboBox1.DisplayMember = "Value";
                comboBox1.ValueMember = "Key";
                comboBox1.Text = "";
                comboBox1.SelectedIndexChanged += comboBox1_SelectedIndexChanged;
                dr3.Close();

                baglanti.Close();
            }
            else
            {

            }
        }

        private void ListView2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView2.SelectedItems.Count > 0)
            {

                textBox2.Clear();
                textBox2.Enabled = true;
                textBox2.Text = listView2.SelectedItems[0].SubItems[3].Text;
                button10.Enabled = true;
            }
            else
            {
                textBox2.Clear();
                button10.Enabled = false;
                textBox2.Enabled = false;


            }

        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {
            serialPort1.Close();
            DemirbasEkle sa = new DemirbasEkle();
            this.Visible = false;
            sa.rolid = rolid;
            sa.bolumid = bolumid;
            sa.ShowDialog();
        }

        private void Label5_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click_1(object sender, EventArgs e)
        {

        }

        private void TextBox2_TextChanged(object sender, EventArgs e)
        {

        }
        private void Button8_Click_1(object sender, EventArgs e)
        {
            //baglanti.Open();
            //SqlCommand komut = new SqlCommand("select mekanID from Urunler as u inner join Mekan as m on m.ID=u.mekanID where u.UrunID=@deger", baglanti);
            //komut.Parameters.AddWithValue("@deger", comboBox2.SelectedValue.ToString());
            //MessageBox.Show(comboBox2.SelectedValue.ToString());
            //int mekanid = (int)komut.ExecuteScalar();
            //baglanti.Close();

            baglanti.Open();
            SqlCommand mekanGüncelle = new SqlCommand("update Urunler set mekanID=@mekanid where UrunID=@urunid", baglanti);
            mekanGüncelle.Parameters.AddWithValue("@mekanid", comboBox2.SelectedValue);
            mekanGüncelle.Parameters.AddWithValue("@urunid", listView1.SelectedItems[0].SubItems[1].Text);
            try
            {
                mekanGüncelle.ExecuteNonQuery();
                baglanti.Close();

                timer3.Start();
               
                //MessageBox.Show("Ürünün bulunduğu konum değiştirildi.", "Bilgilendirme Penceresi", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception asd)
            {
                MessageBox.Show("Ürünün bulunduğu konum değiştirilemedi." + asd);
            }
            if (baglanti.State == ConnectionState.Open)
            {
                baglanti.Close();
            }

            //catch (Exception asd)
            //{
            //    MessageBox.Show(asd.ToString());
            //}

            //int mekanid = 0;
            //baglanti.Open();

            //SqlCommand komut = new SqlCommand("select mekanID from Urunler where UrunID=" + comboBox2.SelectedValue.ToString() , baglanti);
            ////mekanid = (int)komut.ExecuteScalar();
            //SqlDataReader dr = komut.ExecuteReader();
            //if (dr.Read())
            //{
            //    mekanid = Convert.ToInt32(dr[0]);
            //}
            //dr.Close();
            //baglanti.Close();
            //baglanti.Open();
            //int i = 0;
            //baglanti.Open();
            //SqlCommand mekanGüncelle = new SqlCommand("update Urunler set mekanID=@mekanid where UrunID=@urunid", baglanti);
            //mekanGüncelle.Parameters.AddWithValue("@mekanid", mekanid);
            //mekanGüncelle.Parameters.AddWithValue("@urunid", listView1.SelectedItems[0].SubItems[1]);
            //try
            //{
            //    mekanGüncelle.ExecuteNonQuery();
            //    baglanti.Close();
            //    MessageBox.Show("Ürünün bulunduğu konum değiştirildi.", "Bilgilendirme", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //}
            //catch (Exception asd)
            //{
            //    MessageBox.Show("Ürünün bulunduğu konum değiştirilemedi." + asd);
            //}
            //if (baglanti.State == ConnectionState.Open)
            //{
            //    baglanti.Close();
            //}
        }

        private void ComboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox2.SelectedIndex > 0)
            {
                button8.Enabled = true;
            }
            else
            {
                button8.Enabled = false;
            }
        }

        private void PictureBox2_Click(object sender, EventArgs e)
        {
            DialogResult sondurum = MessageBox.Show("Kullanıcı değiştirmek istiyor musunuz ?", "Bilgilendirme Penceresi", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (sondurum == DialogResult.Yes)
            {
                Giris open = new Giris();
                this.Visible = false;
                open.ShowDialog();
            }
        }

        private void button16_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Daha yapılmadı");
        }

        private void button20_Click(object sender, EventArgs e)
        {
            Anasayfa open = new Anasayfa();
            this.Visible = false;
            open.bolumid = bolumid;
            open.rolid = rolid;
            open.ShowDialog();
        }
        string karekod = "";
        private void displayText(object o, EventArgs e)
        {
            string c1text = "";
            string c4text = "";
            comboBox4.Text = "Seçiniz";

            textBox1.Clear();
            textBox1.Text = rxString.ToString();


            listView2.Items.Clear();
            karekod = textBox1.Text.Substring(0, textBox1.Text.Length - 1);

            SqlCommand c1 = new SqlCommand("select kategoriAd,BolumAdı from Urunler as u inner join Kategori as k on k.ID=u.kategoriID " +
                "inner join Bolumler as b on b.BolumID=k.bolumID where u.ürünkodu='" + karekod + "'", baglanti);
            baglanti.Open();
            SqlDataReader dr = c1.ExecuteReader();
            if (dr.Read())
            {
                c1text = dr[0].ToString();
                c4text = dr[1].ToString();
            }
            baglanti.Close();
            comboBox4.Text = c4text;
            comboBox1.Text = c1text;

            /////////
            listView1.Items.Clear();
            SqlCommand kontrol2 = new SqlCommand("select UrunID,ürünkodu from Kategori as k " +
               "inner join Urunler as u on u.kategoriID = k.ID where u.ürünkodu='" + karekod + "'", baglanti);
            baglanti.Open();

            SqlDataReader dr2 = kontrol2.ExecuteReader();
            while (dr2.Read())
            {
                ListViewItem item2 = new ListViewItem(dr2[1].ToString());
                item2.SubItems.Add(dr2[0].ToString());
                listView1.Items.Add(item2);
            }
            baglanti.Close();
            /////////
            SqlCommand kontrol = new SqlCommand("select o.ID,u.urunID,o.ozellikAdı,ozellikDegeri,urun.ürünkodu,urun.kategoriID,k.bolumID from Ozellik as o " +
                "inner join Urün_Ozellik as u on o.ID = u.ozellikID " +
                "inner join Urunler as urun on urun.UrunID = u.urunID " +
                "inner join Kategori as k on k.ID=urun.kategoriID where urun.ürünkodu='" + karekod + "'", baglanti);
            baglanti.Open();

            SqlDataReader dr4 = kontrol.ExecuteReader();
            while (dr4.Read())
            {
                ListViewItem item = new ListViewItem(dr4[0].ToString());
                item.SubItems.Add(dr4[1].ToString());
                item.SubItems.Add(dr4[2].ToString());
                item.SubItems.Add(dr4[3].ToString());
                listView2.Items.Add(item);
            }
            baglanti.Close();

            label7.Text = karekod;
            textBox1.Text = karekod;
            if (listView1.Items.Count>0)
            {
                listView1.Items[0].Selected = true;
            }
            //SqlCommand kontrol2 = new SqlCommand("select UrunID,ürünkodu from Kategori as k " +
            //   "inner join Urunler as u on u.kategoriID = k.ID where k.ID='" + comboBox1.SelectedValue + "'", baglanti);
            //baglanti.Open();

            //SqlDataReader dr = kontrol.ExecuteReader();
            //while (dr.Read())
            //{
            //    ListViewItem item = new ListViewItem(dr[1].ToString());
            //    item.SubItems.Add(dr[0].ToString());
            //    listView1.Items.Add(item);
            //}
        }
        private void serialPort1_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            rxString = serialPort1.ReadExisting();
            this.Invoke(new EventHandler(displayText));
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
        int timer3sayac = 1;
        private void timer3_Tick(object sender, EventArgs e)
        {
            timer3sayac++;
            if (timer3sayac >=0 && timer3sayac <=5)
            {
                label8.Visible = true;
            }
            else
            {
                label8.Visible = false;
                timer3.Stop();
                timer3sayac = 0;
            }
        }
    }

}