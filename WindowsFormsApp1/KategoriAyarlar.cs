﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Configuration;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.Header;
using WindowsFormsApp1.Properties;

namespace WindowsFormsApp1
{
    public partial class KategoriAyarlar : Form
    {
        public KategoriAyarlar()
        {
            InitializeComponent();
        }
        SqlConnection baglanti = new SqlConnection(ConfigurationManager.ConnectionStrings["myConn"].ConnectionString);
        List<string> liste = new List<string>();
        public int rolid { get; set; }
        public int bolumid { get; set; }
        int timerkontrol = 0;
        string id;
        string kontrol = "";

        public void yenile()
        {
            listView1.Items.Clear();
            comboBox1.SelectedIndex = 0;
            comboBox2.SelectedIndex = 0;
        }
        public void listviewyenile()
        {
            listView1.Items.Clear();
            string[] dizi = liste.ToArray();
            int sayac = 0;
            colisimleri = new string[dizi.Length];

            foreach (var item in dizi)
            {
                string[] dizii = item.Split('-');
                colisimleri[sayac] = dizii[0];
                sayac++;
            }
            if (baglanti.State == ConnectionState.Open)
                baglanti.Close();

            SqlCommand kontrol = new SqlCommand("select ozellikID,ozellikAdı from Kategori as k " +
                "inner join KategoriOzellik as i on k.ID=İ.kategoriID " +
                "inner join Ozellik as o on i.ozellikID=o.ID where k.ID = '" + comboBox1.SelectedValue + "' ", baglanti);
            baglanti.Open();

            SqlDataReader dr4 = kontrol.ExecuteReader();
            while (dr4.Read())
            {
                ListViewItem item = new ListViewItem(dr4[1].ToString());
                item.SubItems.Add(dr4[0].ToString());
                listView1.Items.Add(item);
            }
            baglanti.Close();
        }
        private bool isCollabsed = true;

        private void KategoriAyarlar_Load(object sender, EventArgs e)
        {

            if (rolid == 1)
            {
                baglanti.Open();
                SqlCommand komut2 = new SqlCommand("select*from Bolumler", baglanti);
                SqlDataReader dr2 = komut2.ExecuteReader();
                Dictionary<string, string> dis = new Dictionary<string, string>();

                dis.Add("", "Seçiniz");
                while (dr2.Read())
                {
                    dis.Add(dr2.GetValue(0).ToString(), dr2.GetValue(1).ToString());
                }
                baglanti.Close();
                dr2.Close();

                comboBox2.SelectedIndexChanged -= ComboBox2_SelectedIndexChanged;
                comboBox2.Items.Add("Seçiniz");
                comboBox2.DataSource = new BindingSource(dis, null);
                comboBox2.DisplayMember = "Value";
                comboBox2.ValueMember = "Key";
                comboBox2.Text = "Seçiniz";
                comboBox2.SelectedIndexChanged += ComboBox2_SelectedIndexChanged;

                label4.Visible = false;
            }


            if (rolid == 2)
            {
                button20.Enabled = true;
                button2.Enabled = true;
                button4.Enabled = true;
                button5.Visible = false;
                comboBox2.Enabled = false;
                comboBox2.Visible = false;
                comboBox1.Enabled = true;
                button17.Enabled = true;
                baglanti.Open();
                label8.Text = "Bölümünüz: ";
                label8.Location = new Point(67, 35);
                SqlCommand bolumad = new SqlCommand("select BolumAdı from Bolumler as b " +
                   "inner join Kullanici as k on k.Bolumid=b.BolumID where BolumID='" + bolumid + "'", baglanti);

                SqlDataReader dr = bolumad.ExecuteReader();
                if (dr.Read())
                {
                    label4.Text = dr.GetValue(0).ToString();
                }
                dr.Close();
                baglanti.Close();
                baglanti.Open();
                SqlCommand komut3 = new SqlCommand("select ID,kategoriAd from Kategori where BolumID = '" + bolumid + "'", baglanti);
                SqlDataAdapter da2 = new SqlDataAdapter(komut3);
                dr3 = komut3.ExecuteReader();
                Dictionary<string, string> dis2 = new Dictionary<string, string>();

                dis2.Add("", "Seçiniz");
                while (dr3.Read())
                {
                    dis2.Add(dr3.GetValue(0).ToString(), dr3.GetValue(1).ToString());
                }

                comboBox1.SelectedIndexChanged -= comboBox1_SelectedIndexChanged;
                comboBox1.DataSource = new BindingSource(dis2, null);
                comboBox1.DisplayMember = "Value";
                comboBox1.ValueMember = "Key";
                comboBox1.Text = "Seçiniz";
                comboBox1.SelectedIndexChanged += comboBox1_SelectedIndexChanged;
                dr3.Close();
                baglanti.Close();
            }
            else if (rolid == 3)
            {
                button17.Enabled = false;
                button20.Enabled = false;

                button2.Enabled = true;
                button4.Enabled = false;
                button5.Enabled = false;
                button14.Enabled = false;
                comboBox2.Enabled = false;
                comboBox1.Enabled = true;
            }

        }


        public void comboGetir()
        {
            comboBox1.Items.Clear();
            comboBox1.Items.Add("Seçiniz");
            comboBox1.Text = "Seçiniz";
            baglanti.Open();
            DataTable dt = baglanti.GetSchema("Tables");

            baglanti.Close();
        }



        private void button1_Click(object sender, EventArgs e)
        {

        }

        string[] colisimleri;
        string[] colisimleri2;
        //string[] coltipleri;
        //string[] colbosmu;

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            textBox1.Clear();
            label6.Visible = false;
            button12.Visible = false;
            button16.Visible = false;
            textBox3.Visible = false;
            if (comboBox1.SelectedIndex != 0)
            {
                groupBox3.Visible = true;
                groupBox2.Visible = true;
                textBox4.Clear();
                button9.Visible = true;
                button10.Visible = true;
            }
            else
            {
                button3.Enabled = false;
                button10.Visible = false;
                button8.Enabled = false;
                button9.Visible = false;
                textBox4.Clear();
                groupBox2.Visible = false;
                groupBox3.Visible = false;
            }
            if (comboBox1.Text != "Seçiniz")
            {
                string[] dizi = liste.ToArray();
                int sayac = 0;
                colisimleri = new string[dizi.Length];

                foreach (var item in dizi)
                {
                    string[] dizii = item.Split('-');
                    colisimleri[sayac] = dizii[0];
                    sayac++;
                }
                if (baglanti.State == ConnectionState.Open)
                    baglanti.Close();

                SqlCommand kontrol = new SqlCommand("select ozellikID,ozellikAdı from Kategori as k " +
                    "inner join KategoriOzellik as i on k.ID=İ.kategoriID " +
                    "inner join Ozellik as o on i.ozellikID=o.ID where k.ID = '" + comboBox1.SelectedValue + "' ", baglanti);
                baglanti.Open();

                SqlDataReader dr4 = kontrol.ExecuteReader();
                while (dr4.Read())
                {
                    ListViewItem item = new ListViewItem(dr4[1].ToString());
                    item.SubItems.Add(dr4[0].ToString());
                    listView1.Items.Add(item);
                }
                baglanti.Close();


                //////////////////////////////////
                listView2.Items.Clear();
                string[] dizi1 = liste.ToArray();
                int sayac2 = 0;
                colisimleri2 = new string[dizi1.Length];

                foreach (var item in dizi1)
                {
                    string[] dizii = item.Split('-');
                    colisimleri[sayac2] = dizii[0];
                    sayac2++;
                }
                if (baglanti.State == ConnectionState.Open)
                    baglanti.Close();

                SqlCommand kontrol1 = new SqlCommand("select UrunID,kategoriID from Urunler where kategoriID= '" + comboBox1.SelectedValue + "' ", baglanti);
                baglanti.Open();

                SqlDataReader dr5 = kontrol1.ExecuteReader();
                while (dr5.Read())
                {
                    ListViewItem item = new ListViewItem(dr5[1].ToString());
                    item.SubItems.Add(dr5[0].ToString());
                    listView2.Items.Add(item);
                }
                baglanti.Close();

            }

        }

       



        private void button7_Click(object sender, EventArgs e)
        {
            Anasayfa open = new Anasayfa();
            this.Visible = false;
            open.Visible = true;
            open.rolid = rolid;
            open.bolumid = bolumid;
            open.Show();
        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            DialogResult sondurum = MessageBox.Show("Uygulamayı Kapatmak İstiyor musunuz ?", "Bilgilendirme Penceresi", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (sondurum == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DemirbasEkle open = new DemirbasEkle();
            this.Visible = false;
            open.ShowDialog();
            open.rolid = rolid;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            KategoriAyarlar ayar = new KategoriAyarlar();
            this.Hide();
            ayar.Show();
            ayar.rolid = rolid;
        }

        private Point mouse_offset;
        private void panel2_MouseDown(object sender, MouseEventArgs e)
        {
            mouse_offset = new Point(-e.X, -e.Y);
        }

        private void panel2_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                Point mousePos = Control.MousePosition;
                mousePos.Offset(mouse_offset.X, mouse_offset.Y);
                this.Location = mousePos;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //serialPort1.Close();
            Mekan open = new Mekan();
            this.Visible = false;

            open.rolid = rolid;
            open.bolumid = bolumid;
            open.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //serialPort1.Close();
            Kullanıcı open = new Kullanıcı();
            this.Visible = false;

            open.rolid = rolid;
            open.bolumid = bolumid;
            open.ShowDialog();
        }

        private void Button6_Click(object sender, EventArgs e)
        {
            if (timerkontrol == 0)
            {
                timer1.Start();
                button17.Visible = false;
                button1.Visible = false;
                button20.Visible = false;
                button17.Visible = false;
                button2.Visible = false;
                button15.Visible = false;
                button13.Visible = false;
                button14.Visible = false;
                button4.Visible = false;
                button5.Visible = false;
                label1.Visible = false;
            }
            else if (timerkontrol == 1)
            {
                timer1.Start();
            }
        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
            if (timerkontrol == 0)
            {
                flowLayoutPanel2.Width = flowLayoutPanel2.Width - 10;
                if (flowLayoutPanel2.Width == 110)
                {
                    timer1.Stop();
                    timerkontrol = 1;
                }
            }
            else if (timerkontrol == 1)
            {
                flowLayoutPanel2.Width = flowLayoutPanel2.Width + 10;
                if (flowLayoutPanel2.Width == 200)
                {
                    timer1.Stop();
                    timerkontrol = 0;
                    button1.Visible = true;
                    button20.Visible = true;
                    button17.Visible = true;
                    button2.Visible = true;
                    button15.Visible = true;
                    button14.Visible = true;
                    button13.Visible = true;
                    button4.Visible = true;
                    button5.Visible = true;
                    label1.Visible = true;
                }
            }
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
        SqlDataReader dr3;
        private void ComboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            button8.Enabled = false;
            button10.Visible = false;
            textBox1.Clear();
            textBox1.Enabled = false;
            textBox3.Visible = false;
            button9.Visible = false;
            button12.Visible = false;
            button16.Visible = false;
            groupBox3.Visible = false;
            if (comboBox2.SelectedIndex == 0)
            {
                comboBox1.Enabled = false;
                comboBox1.SelectedIndex = 0;
                groupBox2.Visible = false;
            }
            else if (comboBox2.SelectedIndex != 0)
            {
                groupBox2.Visible = false;
                comboBox1.Enabled = true;
            }

            if (comboBox2.SelectedIndex > 0)
            {
                baglanti.Open();
                SqlCommand komut3 = new SqlCommand();
                if (rolid == 1)
                {
                    komut3 = new SqlCommand("select ID,kategoriAd from Kategori where BolumID = '" + comboBox2.SelectedValue + "'", baglanti);
                }
                else if (rolid == 2)
                {
                    komut3 = new SqlCommand("select ID,kategoriAd from Kategori where BolumID = '" + bolumid + "'", baglanti);
                }
                SqlDataAdapter da2 = new SqlDataAdapter(komut3);
                dr3 = komut3.ExecuteReader();
                Dictionary<string, string> dis2 = new Dictionary<string, string>();

                dis2.Add("", "Seçiniz");
                while (dr3.Read())
                {
                    dis2.Add(dr3.GetValue(0).ToString(), dr3.GetValue(1).ToString());
                }

                comboBox1.SelectedIndexChanged -= comboBox1_SelectedIndexChanged;
                comboBox1.DataSource = new BindingSource(dis2, null);
                comboBox1.DisplayMember = "Value";
                comboBox1.ValueMember = "Key";
                comboBox1.Text = "Seçiniz";
                comboBox1.SelectedIndexChanged += comboBox1_SelectedIndexChanged;
                dr3.Close();
                baglanti.Close();
            }
        }
        private void ListView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count > 0)
            {
                textBox1.Enabled = true;
                kontrol = textBox1.Text = listView1.SelectedItems[0].SubItems[0].Text;
                textBox1.Text = listView1.SelectedItems[0].SubItems[0].Text;
                textBox2.Text = listView1.SelectedItems[0].SubItems[1].Text;
                button3.Enabled = true;
            }
            else
            {
                textBox1.Enabled = false;
                textBox1.Clear();
                button3.Enabled = false;
            }
        }

        private void Button9_Click(object sender, EventArgs e)
        {

        }
        bool durum = false;
        private void Button11_Click(object sender, EventArgs e)
        {
            bool s = false;
            var t0 = textBox4.Text.Split(' ');
            foreach (var item in t0)
            {
                if (item != "")
                {
                    s = true;
                }
            }

            durum = false;
            if (s)
            {


                if (listView1.Items.Count > 0)
                {
                    foreach (ListViewItem kontrol in listView1.Items)
                    {
                        if (textBox4.Text == kontrol.SubItems[0].Text)
                        {
                            durum = true;
                            break;
                        }

                    }
                }
                if (!durum)
                {
                    SqlCommand komut9 = new SqlCommand("insert into Ozellik(ozellikAdı)values(@özellik)", baglanti);
                    komut9.Parameters.AddWithValue("@özellik", textBox4.Text);
                    try
                    {
                        baglanti.Open();
                        komut9.ExecuteNonQuery();
                        MessageBox.Show("Yeni özellik başarıyla eklendi.", "Bilgilendirme Penceresi",MessageBoxButtons.OK,MessageBoxIcon.Information);
                        baglanti.Close();


                        baglanti.Open();
                        // ilişki tablosunada ekletebilmem için özellikid yi çekiyorum
                        SqlCommand ozellikid = new SqlCommand("select * from Ozellik where ozellikAdı = '" + textBox4.Text + "'", baglanti);
                        SqlDataReader dr = ozellikid.ExecuteReader();
                        //


                        //ilişki tablosuna ekletme sorgusu
                        if (dr.Read())
                        {
                            id = dr[0].ToString();
                        }
                        SqlCommand komut8 = new SqlCommand("insert into KategoriOzellik(ozellikID,kategoriID) values(@ozellik,@kategori)", baglanti);
                        komut8.Parameters.AddWithValue("@kategori", comboBox1.SelectedValue.ToString());
                        komut8.Parameters.AddWithValue("@ozellik", id);
                        //
                        dr.Close();
                        baglanti.Close();
                        try
                        {
                            baglanti.Open();
                            komut8.ExecuteNonQuery();
                            baglanti.Close();

                        }
                        catch (Exception)
                        {
                            baglanti.Close();
                        }


                    }
                    catch (Exception hata)
                    {
                        baglanti.Close();
                        MessageBox.Show("Özellik eklenemedi. " + hata.Message);
                    }
                }
                else
                {
                    MessageBox.Show("Aynı isimde 2 özellik ekletemezsiniz." ,"Bilgilendirme Penceresi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }


                //listviewi yenilemek için
                listviewyenile();
                //
                textBox4.Clear();


            }
            else
            {
                MessageBox.Show("Lütfen bir özellik adı girin.", "Bilgilendirme Penceresi",MessageBoxButtons.OK,MessageBoxIcon.Information);
            }
        }

        private void Button12_Click(object sender, EventArgs e)
        {


        }



        private void Button18_Click(object sender, EventArgs e)
        {
            timer2.Start();
        }
        private void Button3_Click_1(object sender, EventArgs e)
        {
            if (kontrol != textBox1.Text)
            {
                if (listView1.Items.Count > 0)
                {
                    SqlCommand komut = new SqlCommand("update Ozellik set ozellikAdı=@ozellik where ID=@ID", baglanti);
                    komut.Parameters.AddWithValue("@ozellik", textBox1.Text);
                    komut.Parameters.AddWithValue("@ID", textBox2.Text);

                    try
                    {
                        baglanti.Open();
                        komut.ExecuteNonQuery();
                        baglanti.Close();
                        MessageBox.Show("Seçilen özellik güncellendi.", "Bilgilendirme Penceresi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (Exception asd)
                    {
                        MessageBox.Show("Seçilen özellik güncellenemedi." + asd);
                        throw;
                    }
                    listviewyenile();
                    textBox1.Clear();
                }
            }

            else
            {
                MessageBox.Show("Özelliğin adını değiştirmediniz.","Bilgilendirme", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }


        private void Timer2_Tick_1(object sender, EventArgs e)
        {
            if (isCollabsed)
            {
                button15.Image = Resources.up_arrow;
                panelDropDown.Height += 10;
                if (panelDropDown.Size == panelDropDown.MaximumSize)
                {
                    timer2.Stop();
                    isCollabsed = false;
                }
            }
            else
            {
                button15.Image = Resources.angle_arrow_down;
                panelDropDown.Height -= 10;
                if (panelDropDown.Size == panelDropDown.MinimumSize)
                {
                    timer2.Stop();
                    isCollabsed = true;
                }
            }
        }

        private void Button14_Click(object sender, EventArgs e)
        {
            DemirbasEkle open = new DemirbasEkle();
            this.Visible = false;
            open.bolumid = bolumid;
            open.rolid = rolid;
            open.ShowDialog();
        }

        private void Button13_Click(object sender, EventArgs e)
        {
            DemirbasAyarlar ayar = new DemirbasAyarlar();
            this.Hide();
            ayar.rolid = rolid;
            ayar.bolumid = bolumid;
            ayar.ShowDialog();
        }

        private void Button15_Click(object sender, EventArgs e)
        {
            timer2.Start();
        }

        private void Button8_Click(object sender, EventArgs e)
        {
            DialogResult cikis;
           
            if (listView1.CheckedItems.Count == 1)
            {
                cikis = MessageBox.Show("Seçilenözelliği silmek istiyor musunuz?", "Bilgilendirme Penceresi", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            }
            else
            {
                cikis = MessageBox.Show("Seçilen özellikleri silmek istiyor musunuz?", "Bilgilendirme Penceresi", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            }
            if (cikis == DialogResult.Yes)
            {

                for (int i = 0; i < listView1.CheckedItems.Count; i++)
                {
                    SqlCommand odev = new SqlCommand("delete from KategoriOzellik where ozellikID= @id", baglanti);
                    odev.Parameters.AddWithValue("@id", listView1.CheckedItems[i].SubItems[1].Text);
                    baglanti.Open();
                    odev.ExecuteNonQuery();
                    baglanti.Close();

                    SqlCommand ozellikid = new SqlCommand("delete from Urün_Ozellik where ozellikID= @id", baglanti);
                    ozellikid.Parameters.AddWithValue("@id", listView1.CheckedItems[i].SubItems[1].Text);
                    baglanti.Open();
                    ozellikid.ExecuteNonQuery();
                    baglanti.Close();

                    SqlCommand ozellik = new SqlCommand("delete from Ozellik where ID=@id", baglanti);
                    ozellik.Parameters.AddWithValue("@id", listView1.CheckedItems[i].SubItems[1].Text);
                    baglanti.Open();
                    ozellik.ExecuteNonQuery();
                    baglanti.Close();
                }
                listView1.Items.Clear();
                listviewyenile();
                textBox1.Clear();
                textBox1.Enabled = false;
                MessageBox.Show("Seçilen özellikler başarıyla silindi.", "Bilgilendirme Penceresi",MessageBoxButtons.OK,MessageBoxIcon.Information);
                if (listView1.CheckedItems.Count >= 1)
                {
                    button8.Enabled = true;
                }
                else if (listView1.CheckedItems.Count == 0)
                {
                    button8.Enabled = false;
                }
                try
                {
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Nesneler Silinemedi.", "Hata.");

                    MessageBox.Show(ex + "", "Hata.");
                }
            }
        }
        private void Button9_Click_1(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex > 0)
            {
                DialogResult sondurum = MessageBox.Show("Eğer bu kategoriyi silerseniz bu kategoriye bağlı olan ÜRÜNLER ve ÜRÜNLERİN ÖZELLİKLERİ bu kategori ile beraber silinecektir." +
                " \nBu kategorinin silinmesini istiyor musunuz ?", "Bilgilendirme Penceresi", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (sondurum == DialogResult.Yes)
                {
                    for (int i = 0; i < listView1.Items.Count; i++)
                    {
                        ///////// kategori özellik tablsoundan özellikid sildiriyorum
                        SqlCommand kategoriozellik = new SqlCommand("delete from KategoriOzellik where ozellikID= @id", baglanti);
                        kategoriozellik.Parameters.AddWithValue("@id", listView1.Items[i].SubItems[1].Text);
                        baglanti.Open();
                        kategoriozellik.ExecuteNonQuery();
                        baglanti.Close();
                        ///

                        ///////urun özellik tablosundan özellik id sildiriyorum
                        SqlCommand urunozellik = new SqlCommand("delete from Urün_Ozellik where ozellikID= @id", baglanti);
                        urunozellik.Parameters.AddWithValue("@id", listView1.Items[i].SubItems[1].Text);
                        baglanti.Open();
                        urunozellik.ExecuteNonQuery();
                        baglanti.Close();
                        ///

                        ///////ozellik tablosundan ozellik sildiriyorum
                        SqlCommand ozellik = new SqlCommand("delete from Ozellik where ID=@id", baglanti);
                        ozellik.Parameters.AddWithValue("@id", listView1.Items[i].SubItems[1].Text);
                        baglanti.Open();
                        ozellik.ExecuteNonQuery();
                        baglanti.Close();
                        ///
                    }



                    for (int a = 0; a < listView2.Items.Count; a++)
                    {
                        SqlCommand urun = new SqlCommand("delete from Urunler where kategoriID= @id", baglanti);
                        urun.Parameters.AddWithValue("@id", listView2.Items[a].SubItems[0].Text);
                        baglanti.Open();
                        urun.ExecuteNonQuery();
                        baglanti.Close();
                    }

                    SqlCommand kategori = new SqlCommand("delete from Kategori where ID= @id", baglanti);
                    kategori.Parameters.AddWithValue("@id", comboBox1.SelectedValue);
                    baglanti.Open();
                    kategori.ExecuteNonQuery();
                    MessageBox.Show("Kategori silindi.","Bilgilendirme Penceresi",MessageBoxButtons.OK,MessageBoxIcon.Information);
                    baglanti.Close();

                    listView1.Items.Clear();
                    listviewyenile();

                    ///////////////////combobox1 'i yenilemek için
                    baglanti.Open();
                    SqlCommand komut3 = new SqlCommand();
                    if (rolid == 1)
                    {
                        komut3 = new SqlCommand("select ID,kategoriAd from Kategori where BolumID = '" + comboBox2.SelectedValue + "'", baglanti);
                    }
                    else if (rolid == 2)
                    {
                        komut3 = new SqlCommand("select ID,kategoriAd from Kategori where BolumID = '" + bolumid + "'", baglanti);
                    }
                    SqlDataAdapter da2 = new SqlDataAdapter(komut3);
                    SqlDataReader dr3 = komut3.ExecuteReader();
                    Dictionary<string, string> dis2 = new Dictionary<string, string>();

                    dis2.Add("", "Seçiniz");
                    while (dr3.Read())
                    {
                        dis2.Add(dr3.GetValue(0).ToString(), dr3.GetValue(1).ToString());
                    }

                    comboBox1.SelectedIndexChanged -= comboBox1_SelectedIndexChanged;
                    comboBox1.DataSource = new BindingSource(dis2, null);
                    comboBox1.DisplayMember = "Value";
                    comboBox1.ValueMember = "Key";
                    comboBox1.Text = "Seçiniz";
                    comboBox1.SelectedIndexChanged += comboBox1_SelectedIndexChanged;
                    dr3.Close();
                    baglanti.Close();
                    //////
                    button9.Visible = false;
                    button10.Visible = false;
                    textBox4.Clear();
                    groupBox2.Visible = false;
                    groupBox3.Visible = false;
                }
            }
        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {
            KategoriEkle ekle = new KategoriEkle();
            ekle.bolumid = bolumid;
            ekle.rolid = rolid;

            this.Hide();
            ekle.Show();
        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
            {
                button3.Enabled = false;
            }
            else
            {
                button3.Enabled = true;
            }
        }

        //string deger;
        private void Button10_Click_1(object sender, EventArgs e)
        {
            
            label6.Location = new Point(490, 68);
            textBox3.Location = new Point(600, 65);
            label6.Visible = true;
            button10.Visible = false;
            button12.Visible = true;
           
            textBox3.Visible = true;
            textBox3.Text = comboBox1.Text;
            button9.Visible = false;
            button16.Visible = true;


        }
        int index;
        private void Button12_Click_1(object sender, EventArgs e)
        {

            label6.Visible = false;

            SqlCommand komut = new SqlCommand("update Kategori set kategoriAd=@ad where ID=@id", baglanti);
            komut.Parameters.AddWithValue("@id", comboBox1.SelectedValue);
            komut.Parameters.AddWithValue("@ad", textBox3.Text);
            index = comboBox1.SelectedIndex;
            try
            {
                baglanti.Open();
                komut.ExecuteNonQuery();
                baglanti.Close();

                if (comboBox2.SelectedIndex > 0 || comboBox2.Visible == false)
                {
                    baglanti.Open();
                    SqlCommand komut3 = new SqlCommand();
                    if (rolid == 1)
                    {
                        komut3 = new SqlCommand("select ID,kategoriAd from Kategori where BolumID = '" + comboBox2.SelectedValue + "'", baglanti);
                    }
                    else if (rolid == 2)
                    {
                        komut3 = new SqlCommand("select ID,kategoriAd from Kategori where BolumID = '" + bolumid + "'", baglanti);
                    }
                    SqlDataAdapter da2 = new SqlDataAdapter(komut3);
                    dr3 = komut3.ExecuteReader();
                    Dictionary<string, string> dis2 = new Dictionary<string, string>();

                    dis2.Add("", "Seçiniz");
                    while (dr3.Read())
                    {
                        dis2.Add(dr3.GetValue(0).ToString(), dr3.GetValue(1).ToString());
                    }

                    comboBox1.SelectedIndexChanged -= comboBox1_SelectedIndexChanged;
                    comboBox1.DataSource = new BindingSource(dis2, null);
                    comboBox1.DisplayMember = "Value";
                    comboBox1.ValueMember = "Key";
                    comboBox1.Text = "Seçiniz";
                    comboBox1.SelectedIndexChanged += comboBox1_SelectedIndexChanged;
                    dr3.Close();
                    comboBox1.SelectedIndex = index;
                    MessageBox.Show("Kategorinin adı güncellendi.", "Bilgilendirme Penceresi", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
            }
            catch (Exception)
            {
                baglanti.Close();
            }
        }
        private void ComboBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void Button16_Click(object sender, EventArgs e)
        {
          
            label6.Visible = false;
            textBox3.Visible = false;
            button12.Visible = false;
            button16.Visible = false;
            button9.Visible = true;
            button10.Visible = true;
        }

        private void ListView1_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if (listView1.CheckedItems.Count >= 1)
            {
                button8.Enabled = true;
            }
            else if (listView1.CheckedItems.Count == 0)
            {
                button8.Enabled = false;
            }
        }

        private void Button5_Click_1(object sender, EventArgs e)
        {
            Bolumler open = new Bolumler();
            this.Hide();

            open.rolid = rolid;
            open.bolumid = bolumid;
            open.ShowDialog();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {

        }

        private void pictureBox7_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void PictureBox2_Click(object sender, EventArgs e)
        {
            DialogResult sondurum = MessageBox.Show("Kullanıcı değiştirmek istiyor musunuz ?", "Bilgilendirme Penceresi", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (sondurum == DialogResult.Yes)
            {
                Giris open = new Giris();
                this.Visible = false;
                open.ShowDialog();
            }
        }

        private void button17_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Daha yapılmadı");
        }

        private void button20_Click(object sender, EventArgs e)
        {
            Anasayfa open = new Anasayfa();
            this.Visible = false;
            open.bolumid = bolumid;
            open.rolid = rolid;
            open.ShowDialog();
        }
    }
}
