﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Configuration;
using WindowsFormsApp1.Properties;

namespace WindowsFormsApp1
{
    public partial class KategoriEkle : Form
    {
        public KategoriEkle()
        {
            InitializeComponent();
        }
        int timerkontrol = 0;
        public int rolid { get; set; }

        int k_id;
        public int bolumid { get; set; }
        private bool isCollabsed = true;
        private void getir()
        {
            baglanti.Open();
            SqlCommand komut2 = new SqlCommand("select*from Bolumler", baglanti);
            SqlDataAdapter da = new SqlDataAdapter(komut2);
            SqlDataReader dr2 = komut2.ExecuteReader();
            Dictionary<string, string> dis = new Dictionary<string, string>();

            dis.Add("", "Seçiniz");
            while (dr2.Read())
            {
                dis.Add(dr2.GetValue(0).ToString(), dr2.GetValue(1).ToString());
            }
            comboBox1.DataSource = new BindingSource(dis, null);
            comboBox1.DisplayMember = "Value";
            comboBox1.ValueMember = "Key";
            baglanti.Close();
            dr2.Close();
        }
        SqlConnection baglanti = new SqlConnection(ConfigurationManager.ConnectionStrings["myConn"].ConnectionString);

        private void Kategori_Load(object sender, EventArgs e)
        {
            SqlCommand sorgu = new SqlCommand("select Ad  from Kullanici where Rolid=@rolid", baglanti);
            sorgu.Parameters.AddWithValue("@rolid", rolid);

            baglanti.Open();
            string sonuc = (string)sorgu.ExecuteScalar();
            baglanti.Close();

            if (rolid == 1)
                getir();

            if (rolid == 2)
            {
                label2.Visible = false;
                label6.Visible = true;
                label7.Visible = true;
                comboBox1.Visible = false;
                button15.Enabled = true;
                button11.Enabled = true;
                button2.Enabled = true;
                button4.Enabled = true;
                button5.Visible = false;
                button16.Enabled = true;


                baglanti.Open();
                SqlCommand bolumad = new SqlCommand("select BolumAdı from Bolumler as b " +
                    "inner join Kullanici as k on k.Bolumid=b.BolumID where BolumID='" + bolumid + "'", baglanti);

                SqlDataReader dr = bolumad.ExecuteReader();
                if (dr.Read())
                {
                    label7.Text = dr.GetValue(0).ToString();
                }
                dr.Close();
                baglanti.Close();
            }
            else if (rolid == 3)
            {
                button8.Enabled = false;
                button11.Enabled = false;
                button15.Enabled = true;
                button2.Enabled = true;
                button14.Enabled = false;
                button4.Enabled = false;
                button16.Enabled = false;
            }

        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            DialogResult sondurum = MessageBox.Show("Uygulamayı Kapatmak İstiyor musunuz ?", "Bilgilendirme Penceresi", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (sondurum == DialogResult.Yes)
            {
                Application.Exit();
            }
        }


        private void button8_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (timerkontrol == 0)
            {
                timer1.Start();
                button8.Visible = false;
                button5.Visible = false;
                button2.Visible = false;
                button14.Visible = false;
                button11.Visible = false;
                button16.Visible = false;
                button13.Visible = false;
                button15.Visible = false;
                button4.Visible = false;
                label1.Visible = false;
                button16.Visible = false;
            }
            else if (timerkontrol == 1)
            {
                timer1.Start();

            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (timerkontrol == 0)
            {
                flowLayoutPanel1.Width = flowLayoutPanel1.Width - 10;
                if (flowLayoutPanel1.Width == 110)
                {
                    timer1.Stop();
                    timerkontrol = 1;
                }
            }
            else if (timerkontrol == 1)
            {
                flowLayoutPanel1.Width = flowLayoutPanel1.Width + 10;
                if (flowLayoutPanel1.Width == 200)
                {
                    timer1.Stop();
                    timerkontrol = 0;
                    button8.Visible = true;
                    button16.Visible = true;
                    button11.Visible = true;
                    button5.Visible = true;
                    button4.Visible = true;
                    button15.Visible = true;
                    button14.Visible = true;
                    button13.Visible = true;
                    button2.Visible = true;
                    label1.Visible = true;
                }
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Anasayfa open = new Anasayfa();
            this.Visible = false;
            open.bolumid = bolumid;
            open.rolid = rolid;
            open.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DemirbasEkle open = new DemirbasEkle();
            this.Visible = false;

            open.rolid = rolid;
            open.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Kullanıcı open = new Kullanıcı();
            this.Visible = false;
            open.bolumid = bolumid;
            open.rolid = rolid;
            open.ShowDialog();
        }
        private Point mouse_offset;
        private void panel2_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                Point mousePos = Control.MousePosition;
                mousePos.Offset(mouse_offset.X, mouse_offset.Y);
                this.Location = mousePos;
            }
        }

        private void panel2_MouseDown(object sender, MouseEventArgs e)
        {
            mouse_offset = new Point(-e.X, -e.Y);
        }


        bool durum = false;

        RichTextBox texta = new RichTextBox();
        Label labea = new Label();
        List<Control> kontroller = new List<Control>();
        CheckBox ch2 = new CheckBox();
        private void button6_Click(object sender, EventArgs e)
        {

            if (comboBox1.SelectedIndex <= 0 && rolid == 1)
            {
                MessageBox.Show("Bölüm seçiniz.", "Bilgilendirme Penceresi", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            else if (textBox1.Text == "" && textBox7.Text == "")
            {
                MessageBox.Show("Kategori adı ve Kategori kısltma kodu boş bırakılamaz.", "Bilgilendirme Penceresi", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            else if (textBox1.Text == "")
            {
                MessageBox.Show("Kategori adı boş bırakılamaz.", "Bilgilendirme Penceresi", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else if (textBox7.Text == "")
            {
                MessageBox.Show("Kategori kısaltma kodu boş bırakılamaz.", "Bilgilendirme Penceresi", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                if (textBox7.Text.Length <= 2)
                {
                    MessageBox.Show("Bu alana 3 den az karekter yazılamaz", "Bilgilendirme Penceresi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    if (listBox1.Items.Count > 0)
                    {
                        foreach (ListViewItem kontrol in listBox1.Items)
                        {
                            if (textBox1.Text == kontrol.SubItems[0].Text)
                            {
                                durum = true;
                                break;
                            }

                        }
                    }
                    if (!durum)
                    {
                        groupBox2.Visible = true;
                        button6.Enabled = false;
                    }
                }
            }
        }




        private void GroupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void Button5_Click(object sender, EventArgs e)
        {

            Bolumler open = new Bolumler();
            this.Visible = false;
            open.rolid = rolid;
            open.bolumid = bolumid;
            open.ShowDialog();
        }

        private void Button10_Click(object sender, EventArgs e)
        {

        }

        private void ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            groupBox2.Visible = false;
            if (comboBox1.SelectedIndex == 0)
            {
                textBox1.Enabled = false;
                textBox7.Enabled = false;
                textBox1.Text = "";
                textBox7.Text = "";
            }
            else if (comboBox1.SelectedIndex != 0)
            {
                textBox1.Enabled = true;
                textBox7.Enabled = true;
            }
        }

        private void Button14_Click(object sender, EventArgs e)
        {
            DemirbasEkle open = new DemirbasEkle();
            this.Visible = false;
            open.bolumid = bolumid;
            open.rolid = rolid;
            open.ShowDialog();
        }

        private void Button13_Click(object sender, EventArgs e)
        {
            DemirbasAyarlar ayar = new DemirbasAyarlar();
            this.Hide();
            ayar.rolid = rolid;
            ayar.bolumid = bolumid;
            ayar.ShowDialog();
        }

        private void Timer2_Tick(object sender, EventArgs e)
        {
            if (isCollabsed)
            {
                button15.Image = Resources.up_arrow;
                panelDropDown.Height += 10;
                if (panelDropDown.Size == panelDropDown.MaximumSize)
                {
                    timer2.Stop();
                    isCollabsed = false;
                }
            }
            else
            {
                button15.Image = Resources.angle_arrow_down;
                panelDropDown.Height -= 10;
                if (panelDropDown.Size == panelDropDown.MinimumSize)
                {
                    timer2.Stop();
                    isCollabsed = true;
                }
            }
        }

        private void Button15_Click(object sender, EventArgs e)
        {
            timer2.Start();
        }

        private void TextBox7_TextChanged(object sender, EventArgs e)
        {



        }

        private void GroupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void TextBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void Button3_Click_1(object sender, EventArgs e)
        {
            bool durum = false;
            if (txtozellik.Text != "")
            {
                if (listBox1.Items.Count > 0)
                {
                    foreach (var kontrol in listBox1.Items)
                    {
                        if (txtozellik.Text == kontrol.ToString())
                        {
                            durum = true;
                        }

                    }
                }
                if (!durum)
                {
                    listBox1.Items.Add(txtozellik.Text);
                    button9.Enabled = true;
                }
                else
                {
                    MessageBox.Show("Aynı isimde 2 özellik ekletemezsiniz.", "Bilgilendirme Penceresi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                MessageBox.Show("Lütfen bir özellik adı girin.", "Bilgilendirme Penceresi", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            txtozellik.Clear();
            txtozellik.Focus();
        }

        private void ListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }


        int satirno;
        private void Button10_Click_1(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex >= 0)
            {
                satirno = listBox1.SelectedIndex;
                listBox1.Items.RemoveAt(satirno);
            }
            if (listBox1.Items.Count == 0)
            {
                button9.Enabled = false;
            }
        }
        private void button9_Click(object sender, EventArgs e)
        {

            int bos = 0;
            if (bos > 0)
                MessageBox.Show("Alanları boş bırakmayınız.", "Bilgilendirme Penceresi", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            else
            {
                if (textBox1.Text == "" || textBox7.Text == "")
                    MessageBox.Show("Kategori adını veya kısaltma kodunu lütfen boş bırakmayınız.", "Bilgilendirme Penceresi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                else
                {
                    int kodvarmi = 0;
                    SqlCommand kodbak = new SqlCommand("select ad from kodlar", baglanti);
                    baglanti.Open();
                    SqlDataReader kobak = kodbak.ExecuteReader();
                    while (kobak.Read())
                    {
                        if (textBox7.Text == kobak["ad"].ToString())
                            kodvarmi++;
                    }
                    kobak.Close();
                    if (kodvarmi > 0)
                    {
                        MessageBox.Show("Bu kısaltma zaten kullanımda.", "Bilgilendirme Penceresi", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        baglanti.Close();
                    }
                    else
                    {
                        if (baglanti.State == ConnectionState.Open)
                            baglanti.Close();

                        string sorgu = "insert into Kategori(kategoriAd,kisaKod,bolumID)values(@kategoriad,@kisaKod,@bolumID)";
                        SqlCommand komut = new SqlCommand(sorgu, baglanti);

                        komut.Parameters.AddWithValue("@kategoriad", textBox1.Text);
                        komut.Parameters.AddWithValue("@kisaKod", textBox7.Text);
                        if (rolid == 1)
                        {
                            komut.Parameters.AddWithValue("@bolumID", comboBox1.SelectedValue);
                        }
                        if (rolid == 2)
                        {
                            komut.Parameters.AddWithValue("bolumID", bolumid);
                        }
                        try
                        {
                            baglanti.Open();
                            komut.ExecuteNonQuery();
                            baglanti.Close();


                        }
                        catch (Exception hata)
                        {
                            baglanti.Close();
                            MessageBox.Show("Kaydınız Veri Tabanına eklenememiştir " + hata.Message);
                        }

                        //özellik kısmı yapıldı.

                        baglanti.Open();
                        SqlCommand komut4;
                        SqlCommand komut3 = new SqlCommand("select * from Kategori where kategoriAd = '" + textBox1.Text + "'", baglanti);

                        SqlDataReader dr = komut3.ExecuteReader();

                        SqlCommand komut2;
                        if (dr.Read())
                        {
                            k_id = (int)dr[0];
                        }
                        dr.Close();
                        baglanti.Close();

                        baglanti.Open();
                        foreach (var item in listBox1.Items)
                        {
                            string eklemeöz = "insert into Ozellik(ozellikAdı)values(@özellik)";
                            SqlCommand komut1 = new SqlCommand(eklemeöz, baglanti);
                            komut1.Parameters.AddWithValue("@özellik", item.ToString());
                            komut1.ExecuteNonQuery();

                            komut4 = new SqlCommand("select * from Ozellik where ozellikAdı = '" + item + "'", baglanti);
                            SqlDataReader dr2 = komut4.ExecuteReader();

                            if (dr2.Read())
                            {
                                komut2 = new SqlCommand("insert into KategoriOzellik(kategoriID, ozellikID)values(@kategoriID, @ozellikID)", baglanti);


                                komut2.Parameters.AddWithValue("@kategoriID", k_id.ToString());
                                komut2.Parameters.AddWithValue("@ozellikID", dr2[0].ToString());
                                dr2.Close();

                                komut2.ExecuteNonQuery();
                            }
                            dr2.Close();

                        }
                        baglanti.Close();

                        try//Özellik ekleme kısmı yapıldı.
                        {
                            MessageBox.Show("Katagori Eklenmiştir.", "Bilgilendirme Penceresi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        catch (Exception hata)
                        {
                            baglanti.Close();
                            MessageBox.Show("Kaydınız Veri Tabanına eklenmemiştir " + hata.ToString());
                        }
                        listBox1.Items.Clear();
                        groupBox2.Visible = false;
                        button6.Enabled = true;
                        textBox1.Clear();
                        textBox7.Clear();
                        button9.Enabled = false;

                    }
                }
            }
        }

        private void button12_Click(object sender, EventArgs e)
        {
            KategoriAyarlar ayar = new KategoriAyarlar();
            this.Visible = false;
            ayar.rolid = rolid;
            ayar.bolumid = bolumid;
            ayar.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Mekan sa = new Mekan();
            this.Visible = false;
            sa.rolid = rolid;
            sa.bolumid = bolumid;
            sa.ShowDialog();
        }

        private void pictureBox7_Click(object sender, EventArgs e)
        {

            this.WindowState = FormWindowState.Minimized;
        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {
            DialogResult sondurum = MessageBox.Show("Kullanıcı değiştirmek istiyor musunuz ?", "Uyarı", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (sondurum == DialogResult.Yes)
            {
                Giris open = new Giris();
                this.Visible = false;
                open.ShowDialog();
            }
        }

        private void button16_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Daha yapılmadı");
        }

        private void button11_Click(object sender, EventArgs e)
        {
            Anasayfa open = new Anasayfa();
            this.Visible = false;
            open.bolumid = bolumid;
            open.rolid = rolid;
            open.ShowDialog();
        }
    }
}
