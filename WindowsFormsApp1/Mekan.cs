﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Configuration;
using WindowsFormsApp1.Properties;

namespace WindowsFormsApp1
{
    public partial class Mekan : Form
    {
        SqlConnection baglanti = new SqlConnection(ConfigurationManager.ConnectionStrings["myConn"].ConnectionString);
        public Mekan()
        {

            InitializeComponent();

        }
        int timerkontrol = 0;
        public int rolid { get; set; }

        public int bolumid { get; set; }

        private bool isCollabsed = true;
        public void comboGetir()
        {
            baglanti.Open();
            SqlCommand komut = new SqlCommand("select * from Bolumler", baglanti);
            SqlDataAdapter da = new SqlDataAdapter(komut);
            SqlDataReader dr = komut.ExecuteReader();
            Dictionary<string, string> dis = new Dictionary<string, string>();

            dis.Add("", "Seçiniz");
            while (dr.Read())
            {
                dis.Add(dr.GetValue(0).ToString(), dr.GetValue(1).ToString());
            }
            comboBox1.DataSource = new BindingSource(dis, null);
            comboBox1.DisplayMember = "Value";
            comboBox1.ValueMember = "Key";
            dr.Close();
            baglanti.Close();



        }
        private void Mekan_Load(object sender, EventArgs e)
        {

            listView1.Columns.Add("Mekan Adı", 160);
            listView1.Columns.Add("ID", 0);
            listView1.Columns.Add("Bolüm Adı", 250);
            listView1.CheckBoxes = true;
            textBox2.Enabled = false;
            button8.Enabled = false;
            listView1.FullRowSelect = true;
            Getir();
            if (rolid == 1)
            {
                comboGetir();
                Getir();
                label5.Visible = false;
            }

            if (rolid == 2)
            {
                button9.Enabled = true;
                textBox2.Enabled = true;



                listView1.Items.Clear();

                SqlCommand komut = new SqlCommand("select ID,Yer, t2.BolumAdı from Mekan t1 inner join Bolumler t2 on t1.BolumID=t2.BolumID order by BolumAdı,Yer ASC", baglanti);
                baglanti.Open();
                SqlDataReader dr = komut.ExecuteReader();
                while (dr.Read())
                {

                    ListViewItem item = new ListViewItem(dr["Yer"].ToString());
                    item.SubItems.Add(dr["ID"].ToString());
                    item.SubItems.Add(dr["BolumAdı"].ToString());

                    listView1.Items.Add(item);

                }
                dr.Close();
                baglanti.Close();
            }
            else
            {
                textBox2.Enabled = false;
                listView1.Items.Clear();
                button9.Enabled = false;
            }

            SqlCommand sorgu = new SqlCommand("select Ad  from Kullanici where Rolid=@rolid", baglanti);
            sorgu.Parameters.AddWithValue("@rolid", rolid);

            baglanti.Open();
            string sonuc = (string)sorgu.ExecuteScalar();
            baglanti.Close();


            if (rolid == 2)
            {
                button16.Enabled = true;
                button20.Enabled = true;
                button1.Enabled = true;
                button3.Enabled = true;
                button4.Enabled = true;
                button10.Visible = false;
                comboBox1.Visible = false;
                label4.Text = "Bölümünüz :";
                label4.Location = new Point(40, 45);
                textBox2.Enabled = true;
                if (label4.Text == "Bölümünüz :")
                {
                    button9.Enabled = true;
                }

                ///////comboboxa getirtme
                baglanti.Open();
                SqlCommand bolumad = new SqlCommand("select BolumAdı from Bolumler as b " +
                    "inner join Kullanici as k on k.Bolumid=b.BolumID where BolumID='" + bolumid + "'", baglanti);

                SqlDataReader dr = bolumad.ExecuteReader();
                if (dr.Read())
                {
                    label5.Text = dr.GetValue(0).ToString();
                }
                dr.Close();
                baglanti.Close();
                //

                //////listviewe getirtme
                listView1.Items.Clear();
                SqlCommand komut = new SqlCommand("select ID,Yer, t2.BolumAdı from Mekan t1 " +
                    "inner join Bolumler t2 on t1.BolumID=t2.BolumID where t1.BolumID='" + bolumid + "' order by BolumAdı,Yer ASC", baglanti);


                baglanti.Open();

                SqlDataReader dr2 = komut.ExecuteReader();
                while (dr2.Read())
                {

                    ListViewItem item = new ListViewItem(dr2["Yer"].ToString());
                    item.SubItems.Add(dr2["ID"].ToString());
                    item.SubItems.Add(dr2["BolumAdı"].ToString());
                    listView1.Items.Add(item);

                }
                dr.Close();
                baglanti.Close();
                //
            }
            else if (rolid == 3)
            {
                button16.Enabled = false;
                button20.Enabled = false;
                button1.Enabled = false;
                button3.Enabled = true;
                button4.Enabled = false;
                button10.Enabled = false;
                button11.Enabled = false;
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
        }
        private void button3_Click(object sender, EventArgs e)
        {
            DemirbasEkle sa = new DemirbasEkle();
            this.Visible = false;
            sa.rolid = rolid;
            sa.ShowDialog();
        }
        private void Getir()
        {
            listView1.Items.Clear();
            SqlCommand komut = new SqlCommand("select ID,Yer, t2.BolumAdı from Mekan t1 inner join Bolumler t2 on t1.BolumID=t2.BolumID order by BolumAdı,Yer ASC ", baglanti);


            baglanti.Open();

            SqlDataReader dr = komut.ExecuteReader();
            while (dr.Read())
            {

                ListViewItem item = new ListViewItem(dr["Yer"].ToString());
                item.SubItems.Add(dr["ID"].ToString());
                item.SubItems.Add(dr["BolumAdı"].ToString());
                listView1.Items.Add(item);

            }
            dr.Close();
            baglanti.Close();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (timerkontrol == 0)
            {
                timer1.Start();

                button16.Visible = false;
                button1.Visible = false;
                button20.Visible = false;
                button16.Visible = false;
                button10.Visible = false;
                button2.Visible = false;
                button3.Visible = false;
                button4.Visible = false;
                button11.Visible = false;
                button12.Visible = false;
                label1.Visible = false;
            }
            else if (timerkontrol == 1)
            {
                timer1.Start();
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (timerkontrol == 0)
            {
                flowLayoutPanel1.Width = flowLayoutPanel1.Width - 10;
                if (flowLayoutPanel1.Width == 110)
                {
                    timer1.Stop();
                    timerkontrol = 1;

                }
            }
            else if (timerkontrol == 1)
            {
                flowLayoutPanel1.Width = flowLayoutPanel1.Width + 10;
                if (flowLayoutPanel1.Width == 200)
                {
                    timer1.Stop();
                    timerkontrol = 0;

                    button1.Visible = true;
                    button20.Visible = true;
                    button16.Visible = true;
                    button10.Visible = true;
                    button4.Visible = true;
                    button3.Visible = true;
                    button2.Visible = true;
                    button11.Visible = true;
                    button12.Visible = true;
                    label1.Visible = true;
                }
            }
        }
        private Point mouse_offset;
        private void panel2_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                Point mousePos = Control.MousePosition;
                mousePos.Offset(mouse_offset.X, mouse_offset.Y);
                this.Location = mousePos;
            }
        }

        private void panel2_MouseDown(object sender, MouseEventArgs e)
        {
            mouse_offset = new Point(-e.X, -e.Y);
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button7_Click(object sender, EventArgs e)
        {
            Anasayfa sa = new Anasayfa();
            this.Visible = false;
            sa.rolid = rolid;
            sa.bolumid = bolumid;
            sa.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            KategoriEkle sa = new KategoriEkle();
            this.Visible = false;
            sa.rolid = rolid;
            sa.bolumid = bolumid;
            sa.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Kullanıcı sa = new Kullanıcı();
            this.Visible = false;
            sa.rolid = rolid;
            sa.bolumid = bolumid;
            sa.ShowDialog();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            DialogResult cikis;
            bool seçmekontrol = false;
            if (listView1.CheckedItems.Count == 1)
            {
                cikis = MessageBox.Show("Seçilen mekanı silmek istiyor musunuz?", "Bilgilendirme Penceresi", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                seçmekontrol = true;
            }
            else
            {
                cikis = MessageBox.Show("Seçilen mekanları silmek istiyor musunuz?", "Bilgilendirme Penceresi", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

            }
            if (cikis == DialogResult.Yes)
            {
                try
                {
                    for (int i = 0; i < listView1.CheckedItems.Count; i++)
                    {
                        SqlCommand odev = new SqlCommand("delete from Mekan where ID= @id", baglanti);
                        odev.Parameters.AddWithValue("@id", listView1.CheckedItems[i].SubItems[1].Text);
                        baglanti.Open();
                        odev.ExecuteNonQuery();
                        baglanti.Close();

                    }
                    listView1.Items.Clear();
                    SqlCommand komut = new SqlCommand();
                    if (rolid == 1)
                    {
                        komut = new SqlCommand("select ID,Yer, t2.BolumAdı from Mekan t1 inner join Bolumler t2 on t1.BolumID=t2.BolumID  where t1.BolumID = '" + comboBox1.SelectedValue + "' order by BolumAdı,Yer ASC", baglanti);
                    }
                    else if (rolid == 2)
                    {
                        komut = new SqlCommand("select ID,Yer, t2.BolumAdı from Mekan t1 inner join Bolumler t2 on t1.BolumID=t2.BolumID  where t1.BolumID = '" + bolumid + "' order by BolumAdı,Yer ASC", baglanti);
                    }
                    baglanti.Open();
                    SqlDataReader dr = komut.ExecuteReader();
                    while (dr.Read())
                    {

                        ListViewItem item = new ListViewItem(dr["Yer"].ToString());
                        item.SubItems.Add(dr["ID"].ToString());
                        item.SubItems.Add(dr["BolumAdı"].ToString());

                        listView1.Items.Add(item);

                    }
                    dr.Close();
                    baglanti.Close();
                    //Getir();
                    if (seçmekontrol == true)
                    {
                        MessageBox.Show("Seçilen mekan başarıyla silindi.", "Bilgilendirme Penceresi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("Seçilen mekanlar başarıyla silindi.", "Bilgilendirme Penceresi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Seçilen mekanlar silinemedi.", "Hata.");

                    MessageBox.Show(ex + "", "Hata.");
                }
            }
        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            DialogResult sondurum = MessageBox.Show("Uygulamayı Kapatmak İstiyor musunuz ?", "Bilgilendirme Penceresi", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (sondurum == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void listView1_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            label2.Text = "Seçilen Mekan Sayısı: " + listView1.CheckedItems.Count;

            if (listView1.CheckedItems.Count == 0)
            {
                textBox5.Enabled = false;
                button5.Enabled = false;
                button8.Enabled = false;
            }
            else if (listView1.CheckedItems.Count == 1)
            {

                textBox5.Enabled = false;
                button8.Enabled = false;
                button5.Enabled = true;

            }
            else if (listView1.CheckedItems.Count > 1)
            {
                textBox5.Enabled = false;
                button5.Enabled = true;
                button8.Enabled = false;
            }
        }
        string ad = "";
        string kontrol = "";
        private void button8_Click(object sender, EventArgs e)
        {
            guncelle();
        }
        private void guncelle()
        {
            ad = textBox5.Text;


            if (kontrol != ad)
            {

                for (int i = 0; i < 1; i++)
                {
                    SqlCommand guncelle = new SqlCommand("update Mekan set Yer=@yer where ID=@id", baglanti);

                    guncelle.Parameters.AddWithValue("@id", listView1.SelectedItems[i].SubItems[1].Text);
                    guncelle.Parameters.AddWithValue("@yer", textBox5.Text);

                    //try
                    //{
                    baglanti.Open();
                    guncelle.ExecuteNonQuery();
                    baglanti.Close();

                    button5.Enabled = false;
                    button8.Enabled = false;
                    listView1.Items.Clear();
                    SqlCommand komut = new SqlCommand();
                    if (rolid == 1)
                    {
                        komut = new SqlCommand("select ID,Yer, t2.BolumAdı from Mekan t1 inner join Bolumler t2 on t1.BolumID=t2.BolumID  where t1.BolumID = '" + comboBox1.SelectedValue + "' order by BolumAdı,Yer ASC", baglanti);
                    }
                    else if (rolid == 2)
                    {
                        komut = new SqlCommand("select ID,Yer, t2.BolumAdı from Mekan t1 inner join Bolumler t2 on t1.BolumID=t2.BolumID  where t1.BolumID = '" + bolumid + "' order by BolumAdı,Yer ASC", baglanti);
                    }
                    baglanti.Open();
                    SqlDataReader dr = komut.ExecuteReader();
                    while (dr.Read())
                    {

                        ListViewItem item = new ListViewItem(dr["Yer"].ToString());
                        item.SubItems.Add(dr["ID"].ToString());
                        item.SubItems.Add(dr["BolumAdı"].ToString());

                        listView1.Items.Add(item);

                    }
                    dr.Close();
                    baglanti.Close();
                    textBox5.Clear();
                    /////////
                    listView1.Items.Clear();
                    SqlCommand komut2 = new SqlCommand();
                    if (rolid == 1)
                    {
                        komut2 = new SqlCommand("select ID,Yer, t2.BolumAdı from Mekan t1 inner join Bolumler t2 on t1.BolumID=t2.BolumID where t2.BolumID='" + comboBox1.SelectedValue + "' order by BolumAdı,Yer ASC ", baglanti);
                    }
                    else if (rolid == 2)
                    {
                        komut2 = new SqlCommand("select ID,Yer, t2.BolumAdı from Mekan t1 inner join Bolumler t2 on t1.BolumID=t2.BolumID where t2.BolumID='" + bolumid + "' order by BolumAdı,Yer ASC ", baglanti);
                    }


                    baglanti.Open();

                    SqlDataReader dr2 = komut2.ExecuteReader();
                    while (dr2.Read())
                    {
                        ListViewItem item = new ListViewItem(dr2["Yer"].ToString());
                        item.SubItems.Add(dr2["ID"].ToString());
                        item.SubItems.Add(dr2["BolumAdı"].ToString());
                        listView1.Items.Add(item);

                    }
                    dr2.Close();
                    baglanti.Close();
                    /////////
                    listView1.CheckBoxes = true;

                    MessageBox.Show("Seçilen mekan güncellendi.", "Bilgilendirme Penceresi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //}
                    //catch (Exception ex)
                    //{
                    //baglanti.Close();
                    //MessageBox.Show("Kayıt Güncellenemedi.", "Başarısız!");
                    //MessageBox.Show("" + ex);
                    //}
                }
            }
            else
            {
                MessageBox.Show("Mekan adını değiştirmediniz.", "Bilgilendirme", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void listView1_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            //for (int i = 0; i < listView1.SelectedItems.Count; i++)
            //{
            //    textBox5.Text = listView1.SelectedItems[i].SubItems[0].Text;
            //}
            //textBox5.Enabled =true;
            //button8.Enabled = true;


        }

        private void Button10_Click(object sender, EventArgs e)
        {
            Bolumler sa = new Bolumler();
            this.Visible = false;
            sa.rolid = rolid;
            sa.bolumid = bolumid;
            sa.ShowDialog();
        }

        private void ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rolid == 1)
            {

                textBox2.Clear();
                textBox5.Clear();
                button8.Enabled = false;
                button5.Enabled = false;
                if (comboBox1.SelectedIndex > 0)
                {

                    button9.Enabled = true;
                    textBox2.Enabled = true;



                    listView1.Items.Clear();

                    SqlCommand komut = new SqlCommand("select ID,Yer, t2.BolumAdı from Mekan t1 inner join Bolumler t2 on t1.BolumID=t2.BolumID  where t1.BolumID = '" + comboBox1.SelectedValue + "' order by BolumAdı,Yer ASC", baglanti);
                    baglanti.Open();
                    SqlDataReader dr = komut.ExecuteReader();
                    while (dr.Read())
                    {

                        ListViewItem item = new ListViewItem(dr["Yer"].ToString());
                        item.SubItems.Add(dr["ID"].ToString());
                        item.SubItems.Add(dr["BolumAdı"].ToString());

                        listView1.Items.Add(item);

                    }
                    dr.Close();
                    baglanti.Close();

                }
                else
                {
                    textBox2.Enabled = false;
                    listView1.Items.Clear();
                    button9.Enabled = false;
                }
            }


        }

        private void Timer2_Tick(object sender, EventArgs e)
        {
            if (isCollabsed)
            {
                button12.Image = Resources.up_arrow;
                panelDropDown.Height += 10;
                if (panelDropDown.Size == panelDropDown.MaximumSize)
                {
                    timer2.Stop();
                    isCollabsed = false;
                }
            }
            else
            {
                button12.Image = Resources.angle_arrow_down;
                panelDropDown.Height -= 10;
                if (panelDropDown.Size == panelDropDown.MinimumSize)
                {
                    timer2.Stop();
                    isCollabsed = true;
                }
            }
        }

        private void Button11_Click(object sender, EventArgs e)
        {
            DemirbasEkle open = new DemirbasEkle();
            this.Visible = false;
            open.rolid = rolid;
            open.bolumid = bolumid;
            open.ShowDialog();
        }

        private void Button12_Click(object sender, EventArgs e)
        {
            timer2.Start();
        }

        private void Button3_Click_1(object sender, EventArgs e)
        {
            DemirbasAyarlar ayar = new DemirbasAyarlar();
            this.Hide();
            ayar.rolid = rolid;
            ayar.bolumid = bolumid;
            ayar.ShowDialog();
        }

        private void Button9_Click_1(object sender, EventArgs e)
        {
            if (textBox2.Text != "")
            {
                string sorgu = "insert into Mekan(Yer,BolumID)values(@Yer,@id)";
                SqlCommand komut = new SqlCommand(sorgu, baglanti);
                komut.Parameters.AddWithValue("@Yer", textBox2.Text);
                if (rolid == 1)
                {
                    komut.Parameters.AddWithValue("@id", comboBox1.SelectedValue);
                }
                else if (rolid == 2 || rolid == 3)
                {
                    komut.Parameters.AddWithValue("@id", bolumid);
                }
                int kodvarmi = 0;
                SqlCommand kodbak = new SqlCommand();
                if (rolid == 1)
                {
                    kodbak = new SqlCommand("select Yer from Mekan where BolumID = '" + comboBox1.SelectedValue + "'", baglanti);
                }
                else if (rolid == 2)
                {
                    kodbak = new SqlCommand("select Yer from Mekan where BolumID = '" + bolumid + "'", baglanti);
                }
                //SqlCommand kodbak = new SqlCommand("select Yer from Mekan where BolumID = '" + comboBox1.SelectedValue + "'", baglanti);
                baglanti.Open();
                SqlDataReader kobak = kodbak.ExecuteReader();
                while (kobak.Read())
                {

                    if (textBox2.Text.ToUpper() == kobak["Yer"].ToString().ToUpper())
                        kodvarmi++;

                }
                kobak.Close();
                if (kodvarmi > 0)
                {
                    MessageBox.Show("Bu mekan adı zaten kullanımda.", "Bilgilendirme Penceresi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    if (baglanti.State == ConnectionState.Open)
                    {
                        baglanti.Close();
                    }
                }
                else
                {
                    try
                    {
                        if (baglanti.State == ConnectionState.Closed)
                        {
                            baglanti.Open();
                        }
                        komut.ExecuteNonQuery();
                        if (baglanti.State == ConnectionState.Open)
                        {
                            baglanti.Close();
                        }
                        listView1.Items.Clear();
                        SqlCommand komut2 = new SqlCommand();
                        if (rolid == 1)
                        {
                            komut2 = new SqlCommand("select ID,Yer, t2.BolumAdı from Mekan t1 inner join Bolumler t2 on t1.BolumID=t2.BolumID  where t1.BolumID = '" + comboBox1.SelectedValue + "' order by Yer,BolumAdı ASC", baglanti);
                        }
                        else if (rolid == 2)
                        {
                            komut2 = new SqlCommand("select ID,Yer, t2.BolumAdı from Mekan t1 inner join Bolumler t2 on t1.BolumID=t2.BolumID  where t1.BolumID = '" + bolumid + "' order by Yer,BolumAdı ASC", baglanti);
                        }
                        baglanti.Open();
                        SqlDataReader dr = komut2.ExecuteReader();
                        while (dr.Read())
                        {

                            ListViewItem item = new ListViewItem(dr["Yer"].ToString());
                            item.SubItems.Add(dr["ID"].ToString());
                            item.SubItems.Add(dr["BolumAdı"].ToString());

                            listView1.Items.Add(item);

                        }
                        dr.Close();
                        baglanti.Close();
                        if (checkBox1.Checked == true)
                        {
                            textBox2.Clear();
                        }
                        MessageBox.Show("Mekan eklendi.", "Bilgilendirme Penceresi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //Getir();
                        //MessageBox.Show(textBox2.Text + " Mekanınız  Eklenmiştir.");
                    }
                    catch (Exception hata)
                    {
                        baglanti.Close();
                        MessageBox.Show("Kaydınız Veri Tabanına eklenemememiştir " + hata);
                    }
                }
            }
            else
            {
                MessageBox.Show("Mekan adını girin.", "Bilgilendirme Penceresi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void TextBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count > 0)
            {
                textBox2.Clear();
                textBox5.Enabled = true;
                textBox5.Text = listView1.SelectedItems[0].SubItems[0].Text;
                button8.Enabled = true;
            }
            else
            {
                textBox5.Clear();
                textBox5.Enabled = false;
                button8.Enabled = false;
            }

            kontrol = ad;
        }

        private void PictureBox2_Click(object sender, EventArgs e)
        {
            DialogResult sondurum = MessageBox.Show("Oturumu kapatmak istiyor musunuz ?", "Bilgilendirme Penceresi", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (sondurum == DialogResult.Yes)
            {
                Giris open = new Giris();
                this.Visible = false;
                open.ShowDialog();
            }
        }

        private void button16_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Daha yapılmadı");
        }

        private void button20_Click(object sender, EventArgs e)
        {
            Anasayfa open = new Anasayfa();
            this.Visible = false;
            open.bolumid = bolumid;
            open.rolid = rolid;
            open.ShowDialog();
        }
    }
}
