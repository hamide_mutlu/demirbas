﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Configuration;

namespace WindowsFormsApp1
{
    public partial class Giris : Form
    {
        public Giris()
        {
            InitializeComponent();
        }
        //int kontrol = 0;
        SqlConnection baglanti = new SqlConnection(ConfigurationManager.ConnectionStrings["myConn"].ConnectionString);
        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                Properties.Settings.Default["kuladi"] = textBox1.Text;

            }
            else if (checkBox1.Checked == false)
            {
                Properties.Settings.Default["kuladi"] = "";
            }
            Properties.Settings.Default.Save();

            SqlCommand sorgu = new SqlCommand("select count(ID) from Kullanici where KullaniciAdi=@kadi", baglanti);
            sorgu.Parameters.AddWithValue("@kadi", textBox1.Text);
            baglanti.Open();
            int sonuc = (int)sorgu.ExecuteScalar();
            baglanti.Close();
            if (textBox1.Text == "" && textBox2.Text == "")
            MessageBox.Show("Lütfen Veri Girişi Yapınız.", "Bilgilendirme Penceresi", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            else if (textBox1.Text == "" || textBox2.Text == "")
            MessageBox.Show("Eksik Veri Girişi Yapmayınız.", "Bilgilendirme Penceresi", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            else
            {
                if (sonuc > 0)
                {
                    SqlCommand sorgu2 = new SqlCommand("select count(ID) from Kullanici where KullaniciAdi=@kadi and Sifre=@sifre", baglanti);
                    sorgu2.Parameters.AddWithValue("@kadi", textBox1.Text);
                    sorgu2.Parameters.AddWithValue("@sifre", textBox2.Text);
                    baglanti.Open();
                    int sonuc2 = (int)sorgu2.ExecuteScalar();
                    baglanti.Close();

                    if (sonuc2 > 0)
                    {
                        label1.Visible = true;
                        pictureBox2.Visible = true;
                        timer1.Start();


                        Anasayfa open = new Anasayfa();


                        this.Visible = false;



                        SqlCommand sorgu3 = new SqlCommand("select Rolid ,Bolumid from Kullanici where KullaniciAdi=@kadi and Sifre=@sifre", baglanti);
                        sorgu3.Parameters.AddWithValue("@kadi", textBox1.Text);
                        sorgu3.Parameters.AddWithValue("@sifre", textBox2.Text);


                        baglanti.Open();


                        SqlDataReader dr = sorgu3.ExecuteReader();
                        if (dr.Read())
                        {
                            open.rolid = Convert.ToInt32(dr[0]);
                            open.bolumid = Convert.ToInt32(dr[1]);
                        }


                        baglanti.Close();

                        
                        open.ShowDialog();


                    }
                    else
                    {
                        MessageBox.Show("Yanlış Şifre.", "Bilgilendirme Penceresi", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
                else
                {
                    MessageBox.Show("Böyle Bir Kullanıcı Bulunmamaktadır.", "Bilgilendirme Penceresi", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }



            //    SqlCommand cmd = new SqlCommand("select KulID,sifre from kullanıcı where KulID = @KulID and sifre = @Sifre", conn);
            //    cmd.Parameters.AddWithValue("@KulID", textBox1.Text);
            //    cmd.Parameters.AddWithValue("@Sifre", textBox2.Text);
            //    cmd.Connection.Open();
            //    SqlDataReader rd = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            //    if (rd.HasRows)
            //    {
            //        while (rd.Read())
            //        {
            //            label1.Visible = true;
            //            pictureBox2.Visible = true;
            //            timer1.Start();
            //        }
            //    }

            //    else
            //    {
            //        MessageBox.Show("Kullanici adi veya Sifreniz Yanlis", "Giris Islemi Basarisiz ", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);


            //    }
            //cmd.Connection.Close();
        }

        private void Giris_Load(object sender, EventArgs e)
        {
            textBox1.Text = Properties.Settings.Default["kuladi"].ToString();
            if (textBox1.Text.Count() > 0)
            {
                checkBox1.Checked = true;
                textBox2.Focus();
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
                button1.Enabled = false;
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            if (textBox2.Text == "")
                button1.Enabled = false;
        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            DialogResult sondurum = MessageBox.Show("Uygulamayı Kapatmak İstiyor musunuz ?", "Bilgilendirme Penceresi", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (sondurum == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        int i = 0;
        private void timer1_Tick(object sender, EventArgs e)
        {


            i++;
            if (i == 2)
            {
                label1.Text = "Yükleniyor...";
            }
            if (i == 5)
            {
                timer1.Stop();
            }
        }

        private void linkLabel3_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Sifre open = new Sifre();
            this.Visible = false;
            open.ShowDialog();

        }

        private Point mouse_offset;
        private void panel2_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                Point mousePos = Control.MousePosition;
                mousePos.Offset(mouse_offset.X, mouse_offset.Y);
                this.Location = mousePos;
            }
        }

        private void panel2_MouseDown(object sender, MouseEventArgs e)
        {
            mouse_offset = new Point(-e.X, -e.Y);
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {

        }

        private void CheckBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox2.Checked == true)
                textBox2.PasswordChar = '\0';
            else if (checkBox2.Checked == false)
                textBox2.PasswordChar = '*';
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Giris_Shown(object sender, EventArgs e)
        {
            if(textBox1.Text != "")
            {
                textBox2.Focus();

            }
            else
            {
                textBox1.Focus();
            }

        }
    }
}
